package com.bfh.djevtic.statisticsforbfh.background.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bfh.djevtic.statisticsforbfh.R;
import com.bfh.djevtic.statisticsforbfh.models.UpcomingUnlocks;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by djevtic on 15.4.2015.
 */
public class UpcomingListAdapter extends ArrayAdapter {

    private Context mContext;
    private ArrayList<UpcomingUnlocks> mUpcomingUnlocks;

    public UpcomingListAdapter(Context context, int resource, ArrayList<UpcomingUnlocks> unlocks) {
        super(context, resource, unlocks);
        this.mContext = context;
        this.mUpcomingUnlocks = unlocks;
    }

    private static class ViewHolder {
        TextView name;
        ImageView img;
        TextView description;
        TextView curent;
        TextView needed;
        TextView progress;
        ProgressBar bar;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        UpcomingUnlocks unlocks = mUpcomingUnlocks.get(position);
        if(convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.upcoming_unlock_list_item, null);
            }

            viewHolder = new ViewHolder();

            viewHolder.name = (TextView) convertView.findViewById(R.id.upcoming_item_name);
            viewHolder.img = (ImageView) convertView.findViewById(R.id.upcoming_item_image);
            viewHolder.description = (TextView) convertView.findViewById(R.id.upcoming_item_description);
            viewHolder.curent = (TextView) convertView.findViewById(R.id.upcoming_item_current);
            viewHolder.needed = (TextView) convertView.findViewById(R.id.upcoming_item_need);
            viewHolder.progress = (TextView) convertView.findViewById(R.id.upcoming_item_progress);
            viewHolder.bar = (ProgressBar) convertView.findViewById(R.id.upcoming_item_progress_bar);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        if(unlocks.getmType().equals("weaponStar")){
            viewHolder.name.setText(String.valueOf(unlocks.getmSubName()+" "+unlocks.getmName()));
        }else {
            viewHolder.name.setText(String.valueOf(unlocks.getmName()));
        }

        if(unlocks.getmDescription() == null){
            viewHolder.description.setText(unlocks.getmNName());
        }else{
            viewHolder.description.setText(unlocks.getmDescription());
        }
        viewHolder.curent.setText(String.valueOf(unlocks.getmCurr()));
        viewHolder.needed.setText(String.valueOf(unlocks.getmNeeded()));
        viewHolder.progress.setText(String.valueOf(unlocks.getmProg()));

        viewHolder.bar.setMax(100);
        viewHolder.bar.setProgress(unlocks.getmProg());

        String imgUrl;
        if(unlocks.getmType().equals("weaponStar")){
            if(unlocks.getmSubImage().equals("null")){
                imgUrl = mContext.getString(R.string.dropbox_url) + "bfh/weapons_fancy/" + unlocks.getmImage()+".png";
            }else {
                imgUrl = mContext.getString(R.string.dropbox_url) + "bfh/weapons_fancy/" + unlocks.getmSubImage()+".png";
            }
        }else{
            if(unlocks.getmSubImage().equals("null")){
                imgUrl = mContext.getString(R.string.dropbox_url) + "bfh/medals/"+unlocks.getmImage()+".png";
            }else {
                imgUrl = mContext.getString(R.string.dropbox_url) + "bfh/medals/" + unlocks.getmSubImage()+".png";
            }
        }
        Picasso.with(mContext).load(imgUrl).placeholder(R.drawable.maintenance).into(viewHolder.img);

        return convertView;
    }
}