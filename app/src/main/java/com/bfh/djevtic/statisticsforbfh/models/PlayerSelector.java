package com.bfh.djevtic.statisticsforbfh.models;

/**
 * Created by djevt_000 on 4/17/2015.
 */
public class PlayerSelector {
    String mPLayerName;
    String mPlayerRank;
    String mPlayerFilename;
    String mPlayerURL;

    public String getmPLayerName() {
        return mPLayerName;
    }

    public void setmPLayerName(String mPLayerName) {
        this.mPLayerName = mPLayerName;
    }

    public String getmPlayerRank() {
        return mPlayerRank;
    }

    public void setmPlayerRank(String mPlayerRank) {
        this.mPlayerRank = mPlayerRank;
    }

    public String getmPlayerFilename() {
        return mPlayerFilename;
    }

    public void setmPlayerFilename(String mPlayerFilename) {
        this.mPlayerFilename = mPlayerFilename;
    }

    public String getmPlayerURL() {
        return mPlayerURL;
    }

    public void setmPlayerURL(String mPlayerURL) {
        this.mPlayerURL = mPlayerURL;
    }
}
