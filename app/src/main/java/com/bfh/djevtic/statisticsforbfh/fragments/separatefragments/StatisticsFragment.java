package com.bfh.djevtic.statisticsforbfh.fragments.separatefragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.bfh.djevtic.statisticsforbfh.R;
import com.bfh.djevtic.statisticsforbfh.globals.Globals;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;
import com.squareup.picasso.Picasso;

/**
 * Created by djevt_000 on 4/6/2015.
 */
public class StatisticsFragment extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_statistics_layout, container, false);

        AdView mAdView = (AdView) rootView.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        ImageView curentImg = (ImageView) rootView.findViewById(R.id.statistics_curent_rank_image);
        ImageView nextImg = (ImageView) rootView.findViewById(R.id.statistics_next_rank_image);
        TextView curentLevel = (TextView) rootView.findViewById(R.id.statistics_curent_rant_text);
        TextView nextLevel = (TextView) rootView.findViewById(R.id.statistics_next_rant_text);

        TextView playerName = (TextView) rootView.findViewById(R.id.statistics_username);
        TextView playerTag = (TextView) rootView.findViewById(R.id.statistics_tag);
        ProgressBar playerProgress = (ProgressBar) rootView.findViewById(R.id.statistics_progress_bar);
        TextView progresNeeded = (TextView) rootView.findViewById(R.id.statistics_progress_bar_text);

        TextView score = (TextView) rootView.findViewById(R.id.score_score);
        TextView award = (TextView) rootView.findViewById(R.id.score_award);
        TextView bonus = (TextView) rootView.findViewById(R.id.score_bonus);
        TextView unlocks = (TextView) rootView.findViewById(R.id.score_unlocks);
        TextView vehicle = (TextView) rootView.findViewById(R.id.score_vehicle);
        TextView team = (TextView) rootView.findViewById(R.id.score_team);
        TextView objective = (TextView) rootView.findViewById(R.id.score_objective);
        TextView squad = (TextView) rootView.findViewById(R.id.score_squad);
        TextView hacker = (TextView) rootView.findViewById(R.id.score_hacker);
        TextView general = (TextView) rootView.findViewById(R.id.score_general);
        TextView totalScore = (TextView) rootView.findViewById(R.id.score_total_score);
        TextView rankScore = (TextView) rootView.findViewById(R.id.score_rank_score);
        TextView combatScore = (TextView) rootView.findViewById(R.id.score_combat_score);

        TextView dogtagAdvanceName = (TextView) rootView.findViewById(R.id.dogtag_advance_name);
        TextView dogtagBasicName = (TextView) rootView.findViewById(R.id.dogtag_basic_name);
        ImageView dogtagAdvanceImage = (ImageView) rootView.findViewById(R.id.dogtag_advance_image);
        ImageView dogtagBasicImage = (ImageView) rootView.findViewById(R.id.dogtag_basic_image);
        TextView dogtagAdvanceDescription = (TextView) rootView.findViewById(R.id.dogtag_advance_description);
        TextView dogtagBasicDescription = (TextView) rootView.findViewById(R.id.dogtag_basic_description);

        String advanceDogtagName = "";
        String advanceDogtagSescription = "";
        String advanceDogtagImgUrl = "https://dl.dropboxusercontent.com/u/1480498/bfh/dogtags/ID_P658_IMAGE.png";
        if((Globals.player.getmAdvanceDogtag()!=null)&&(Globals.player.getmAdvanceDogtag().getmName()!=null)){
            advanceDogtagName = Globals.player.getmAdvanceDogtag().getmName();
        }
        if((Globals.player.getmAdvanceDogtag()!=null)&&(Globals.player.getmAdvanceDogtag().getmDescription()!=null)){
            advanceDogtagSescription = Globals.player.getmAdvanceDogtag().getmDescription();
        }
        if((Globals.player.getmAdvanceDogtag()!=null)&&(Globals.player.getmAdvanceDogtag().getmImage()!=null)){
            advanceDogtagImgUrl = getString(R.string.dropbox_url) + Globals.player.getmAdvanceDogtag().getmImage();
        }
        dogtagAdvanceName.setText(advanceDogtagName);
        dogtagAdvanceDescription.setText(advanceDogtagSescription);
        Picasso.with(getActivity().getApplicationContext()).load(advanceDogtagImgUrl).placeholder(R.drawable.maintenance).into(dogtagAdvanceImage);

        String basicDogtagName = "";
        String basicDogtagSescription = "";
        String basicDogtagImgUrl = "https://dl.dropboxusercontent.com/u/1480498/bfh/dogtags/ID_P658_IMAGE.png";
        if((Globals.player.getmBasicDogtag()!=null)&&(Globals.player.getmBasicDogtag().getmName()!=null)){
            basicDogtagName = Globals.player.getmBasicDogtag().getmName();
        }
        if((Globals.player.getmBasicDogtag()!=null)&&(Globals.player.getmBasicDogtag().getmDescription()!=null)){
            basicDogtagSescription = Globals.player.getmBasicDogtag().getmDescription();
        }
        if((Globals.player.getmBasicDogtag()!=null)&&(Globals.player.getmBasicDogtag().getmImage()!=null)){
            basicDogtagImgUrl = getString(R.string.dropbox_url) + Globals.player.getmBasicDogtag().getmImage();
        }
        dogtagBasicName.setText(basicDogtagName);
        dogtagBasicDescription.setText(basicDogtagSescription);
        Picasso.with(getActivity().getApplicationContext()).load(basicDogtagImgUrl).placeholder(R.drawable.maintenance).into(dogtagBasicImage);

        if(Globals.player.getmStatistics() != null) {
            score.setText(String.valueOf((int) Globals.player.getmStatistics().getmScores().getmScore()));
            award.setText(String.valueOf((int) Globals.player.getmStatistics().getmScores().getmAwardScore()));
            bonus.setText(String.valueOf((int) Globals.player.getmStatistics().getmScores().getmBonusScore()));
            unlocks.setText(String.valueOf((int) Globals.player.getmStatistics().getmScores().getmUnlockScore()));
            vehicle.setText(String.valueOf((int) Globals.player.getmStatistics().getmScores().getmVehicleScore()));
            team.setText(String.valueOf((int) Globals.player.getmStatistics().getmScores().getmTeamScore()));
            objective.setText(String.valueOf((int) Globals.player.getmStatistics().getmScores().getmObjectiveScore()));
            squad.setText(String.valueOf((int) Globals.player.getmStatistics().getmScores().getmSquadScore()));
            hacker.setText(String.valueOf((int) Globals.player.getmStatistics().getmScores().getmHackerScore()));
            general.setText(String.valueOf((int) Globals.player.getmStatistics().getmScores().getmGeneralScore()));
            totalScore.setText(String.valueOf((int) Globals.player.getmStatistics().getmScores().getmTotalScore()));
            rankScore.setText(String.valueOf((int) Globals.player.getmStatistics().getmScores().getmRankScore()));
            combatScore.setText(String.valueOf((int) Globals.player.getmStatistics().getmScores().getmCombatScore()));


            Picasso.with(getActivity().getApplicationContext()).load(getString(R.string.dropbox_url) + Globals.player.getmRank().getmImage()).placeholder(R.drawable.maintenance).into(curentImg);
            Picasso.with(getActivity().getApplicationContext()).load(getString(R.string.dropbox_url) + Globals.player.getmRank().getmNextImage()).placeholder(R.drawable.maintenance).into(nextImg);
            curentLevel.setText("Current " + Globals.player.getmRank().getmLevel());
            nextLevel.setText("Next " + Globals.player.getmRank().getmNextLevel());

            playerName.setText(Globals.player.getmName());
            playerTag.setText(Globals.player.getmTAG());

            playerProgress.setMax((int) Globals.player.getmRank().getmRealNeeded());
            playerProgress.setProgress((int) Globals.player.getmRank().getmRealCurrent());

            progresNeeded.setText("Need " + (Globals.player.getmRank().getmRealNeeded() - Globals.player.getmRank().getmRealCurrent()) + " more");
        }
        return  rootView;
    }
}
