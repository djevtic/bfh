package com.bfh.djevtic.statisticsforbfh.models;

/**
 * Created by djevtic on 1.4.2015.
 */
public class ExtrasScore {
    private double mKDR;
    private double mWLR;
    private double mSPM;
    private double mGSPM;
    private double mKPM;
    private double mSFPM;
    private double mHKP;
    private double KHP;
    private double mAccuracy;
    private int mRoundFinished;
    private int mVehicleTime;
    private int mVehicleKills;
    private int mWeaponTime;
    private int mWeaponKills;
    private int mUnknownKills;
    private double mWEATimeP;
    private double mWEAKillsP;
    private double mWEAKPM;
    private double mVehTimeP;
    private double mVehKillsP;
    private double mVehKPM;
    private int mRibbons;
    private int mRibbonsTotal;
    private int mRibbonsUnique;
    private int mMedals;
    private int mMedalsUnique;
    private int mMedalsTotal;
    private int mAssighnments;
    private int mAssighnmentTota;
    private double mRIBPR;

    public double getmKDR() {
        return mKDR;
    }

    public void setmKDR(double mKDR) {
        this.mKDR = mKDR;
    }

    public double getmWLR() {
        return mWLR;
    }

    public void setmWLR(double mWLR) {
        this.mWLR = mWLR;
    }

    public double getmSPM() {
        return mSPM;
    }

    public void setmSPM(double mSPM) {
        this.mSPM = mSPM;
    }

    public double getmGSPM() {
        return mGSPM;
    }

    public void setmGSPM(double mGSPM) {
        this.mGSPM = mGSPM;
    }

    public double getmKPM() {
        return mKPM;
    }

    public void setmKPM(double mKPM) {
        this.mKPM = mKPM;
    }

    public double getmSFPM() {
        return mSFPM;
    }

    public void setmSFPM(double mSFPM) {
        this.mSFPM = mSFPM;
    }

    public double getmHKP() {
        return mHKP;
    }

    public void setmHKP(double mHKP) {
        this.mHKP = mHKP;
    }

    public double getKHP() {
        return KHP;
    }

    public void setKHP(double KHP) {
        this.KHP = KHP;
    }

    public double getmAccuracy() {
        return mAccuracy;
    }

    public void setmAccuracy(double mAccuracy) {
        this.mAccuracy = mAccuracy;
    }

    public int getmRoundFinished() {
        return mRoundFinished;
    }

    public void setmRoundFinished(int mRoundFinished) {
        this.mRoundFinished = mRoundFinished;
    }

    public int getmVehicleTime() {
        return mVehicleTime;
    }

    public void setmVehicleTime(int mVehicleTime) {
        this.mVehicleTime = mVehicleTime;
    }

    public int getmVehicleKills() {
        return mVehicleKills;
    }

    public void setmVehicleKills(int mVehicleKills) {
        this.mVehicleKills = mVehicleKills;
    }

    public int getmWeaponTime() {
        return mWeaponTime;
    }

    public void setmWeaponTime(int mWeaponTime) {
        this.mWeaponTime = mWeaponTime;
    }

    public int getmWeaponKills() {
        return mWeaponKills;
    }

    public void setmWeaponKills(int mWeaponKills) {
        this.mWeaponKills = mWeaponKills;
    }

    public int getmUnknownKills() {
        return mUnknownKills;
    }

    public void setmUnknownKills(int mUnknownKills) {
        this.mUnknownKills = mUnknownKills;
    }

    public double getmWEATimeP() {
        return mWEATimeP;
    }

    public void setmWEATimeP(double mWEATimeP) {
        this.mWEATimeP = mWEATimeP;
    }

    public double getmWEAKillsP() {
        return mWEAKillsP;
    }

    public void setmWEAKillsP(double mWEAKillsP) {
        this.mWEAKillsP = mWEAKillsP;
    }

    public double getmWEAKPM() {
        return mWEAKPM;
    }

    public void setmWEAKPM(double mWEAKPM) {
        this.mWEAKPM = mWEAKPM;
    }

    public double getmVehTimeP() {
        return mVehTimeP;
    }

    public void setmVehTimeP(double mVehTimeP) {
        this.mVehTimeP = mVehTimeP;
    }

    public double getmVehKillsP() {
        return mVehKillsP;
    }

    public void setmVehKillsP(double mVehKillsP) {
        this.mVehKillsP = mVehKillsP;
    }

    public double getmVehKPM() {
        return mVehKPM;
    }

    public void setmVehKPM(double mVehKPM) {
        this.mVehKPM = mVehKPM;
    }

    public int getmRibbons() {
        return mRibbons;
    }

    public void setmRibbons(int mRibbons) {
        this.mRibbons = mRibbons;
    }

    public int getmRibbonsTotal() {
        return mRibbonsTotal;
    }

    public void setmRibbonsTotal(int mRibbonsTotal) {
        this.mRibbonsTotal = mRibbonsTotal;
    }

    public int getmRibbonsUnique() {
        return mRibbonsUnique;
    }

    public void setmRibbonsUnique(int mRibbonsUnique) {
        this.mRibbonsUnique = mRibbonsUnique;
    }

    public int getmMedals() {
        return mMedals;
    }

    public void setmMedals(int mMedals) {
        this.mMedals = mMedals;
    }

    public int getmMedalsUnique() {
        return mMedalsUnique;
    }

    public void setmMedalsUnique(int mMedalsUnique) {
        this.mMedalsUnique = mMedalsUnique;
    }

    public int getmMedalsTotal() {
        return mMedalsTotal;
    }

    public void setmMedalsTotal(int mMedalsTotal) {
        this.mMedalsTotal = mMedalsTotal;
    }

    public int getmAssighnments() {
        return mAssighnments;
    }

    public void setmAssighnments(int mAssighnments) {
        this.mAssighnments = mAssighnments;
    }

    public int getmAssighnmentTota() {
        return mAssighnmentTota;
    }

    public void setmAssighnmentTota(int mAssighnmentTota) {
        this.mAssighnmentTota = mAssighnmentTota;
    }

    public double getmRIBPR() {
        return mRIBPR;
    }

    public void setmRIBPR(double mRIBPR) {
        this.mRIBPR = mRIBPR;
    }
}
