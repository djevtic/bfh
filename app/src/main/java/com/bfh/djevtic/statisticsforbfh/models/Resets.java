package com.bfh.djevtic.statisticsforbfh.models;

/**
 * Created by djevtic on 3.4.2015.
 */
public class Resets {
    private long mLastReset;
    private long mScore;
    private long mTimePLayed;
    private long mTimePlayedSinceLastReset;
    private int mKills;
    private int mDeaths;
    private long mShotsFired;
    private long mShotsHit;
    private int mNumberLose;
    private int mNumberWin;

    public long getmLastReset() {
        return mLastReset;
    }

    public void setmLastReset(long mLastReset) {
        this.mLastReset = mLastReset;
    }

    public long getmScore() {
        return mScore;
    }

    public void setmScore(long mScore) {
        this.mScore = mScore;
    }

    public long getmTimePLayed() {
        return mTimePLayed;
    }

    public void setmTimePLayed(long mTimePLayed) {
        this.mTimePLayed = mTimePLayed;
    }

    public long getmTimePlayedSinceLastReset() {
        return mTimePlayedSinceLastReset;
    }

    public void setmTimePlayedSinceLastReset(long mTimePlayedSinceLastReset) {
        this.mTimePlayedSinceLastReset = mTimePlayedSinceLastReset;
    }

    public int getmKills() {
        return mKills;
    }

    public void setmKills(int mKills) {
        this.mKills = mKills;
    }

    public int getmDeaths() {
        return mDeaths;
    }

    public void setmDeaths(int mDeaths) {
        this.mDeaths = mDeaths;
    }

    public long getmShotsFired() {
        return mShotsFired;
    }

    public void setmShotsFired(long mShotsFired) {
        this.mShotsFired = mShotsFired;
    }

    public long getmShotsHit() {
        return mShotsHit;
    }

    public void setmShotsHit(long mShotsHit) {
        this.mShotsHit = mShotsHit;
    }

    public int getmNumberLose() {
        return mNumberLose;
    }

    public void setmNumberLose(int mNumberLose) {
        this.mNumberLose = mNumberLose;
    }

    public int getmNumberWin() {
        return mNumberWin;
    }

    public void setmNumberWin(int mNumberWin) {
        this.mNumberWin = mNumberWin;
    }
}