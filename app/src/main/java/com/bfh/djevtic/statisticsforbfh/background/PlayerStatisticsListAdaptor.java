package com.bfh.djevtic.statisticsforbfh.background;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bfh.djevtic.statisticsforbfh.R;

/**
 * Created by djevtic on 20.3.2015.
 */
public class PlayerStatisticsListAdaptor extends ArrayAdapter {

    private Context context;
    private boolean useList = true;
    private View mRowView;

    public PlayerStatisticsListAdaptor(Context context, int resource) {
        super(context, resource);
        this.context = context;
    }

    /**
     * Holder for the list items.
     */
    private class ViewHolder{
        TextView titleText;
    }

    public View getView(int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        mRowView = inflater.inflate(R.layout.player_statistics_row, parent, false);
        /* TO-DO */
        return mRowView;
    }
}
