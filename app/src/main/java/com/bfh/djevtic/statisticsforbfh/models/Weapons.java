package com.bfh.djevtic.statisticsforbfh.models;

/**
 * Created by djevtic on 1.4.2015.
 */

public class Weapons  implements Comparable<Weapons> {

    private String mID;
    private int mShoots;
    private int mHits;
    private int mHeadShots;
    private int mKills;
    private long mTime;
    private int mBStart;
    private int mSStars;
    private int mGStars;

    private String mName;
    private String mCategory;
    private String mType;
    private String mDesscription;

    private double mHandling;
    private boolean mFireModeBurst;
    private double mDamage;
    private double mMobility;
    private boolean mFireModeSingle;
    private boolean mFireModeAuto;
    private double mStatRange;
    private String mRange;
    private String mRateOfFire;
    private String mAmmoType;
    private Double mWeaponAccuracy;
    private String mAmmo;
    private String mUnlockLicense;
    private String mUnlockCode;
    private String mUnlockName;
    private int mUnlockNeededl;

    private long mKillPerMinute;
    private double mSFPM; //Shots fired per minute
    private double mHeadKillPercentage;
    private double mAccuracy;

    private String mImageFancy;
    private String mImageLineart;

    public String getmID() {
        return mID;
    }

    public void setmID(String mID) {
        this.mID = mID;
    }

    public int getmShoots() {
        return mShoots;
    }

    public void setmShoots(int mShoots) {
        this.mShoots = mShoots;
    }

    public int getmHits() {
        return mHits;
    }

    public void setmHits(int mHits) {
        this.mHits = mHits;
    }

    public int getmHeadShots() {
        return mHeadShots;
    }

    public void setmHeadShots(int mHeadShots) {
        this.mHeadShots = mHeadShots;
    }

    public int getmKills() {
        return mKills;
    }

    public void setmKills(int mKills) {
        this.mKills = mKills;
    }

    public long getmTime() {
        return mTime;
    }

    public void setmTime(long mTime) {
        this.mTime = mTime;
    }

    public int getmBStart() {
        return mBStart;
    }

    public void setmBStart(int mBStart) {
        this.mBStart = mBStart;
    }

    public int getmSStars() {
        return mSStars;
    }

    public void setmSStars(int mSStars) {
        this.mSStars = mSStars;
    }

    public int getmGStars() {
        return mGStars;
    }

    public void setmGStars(int mGStars) {
        this.mGStars = mGStars;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmCategory() {
        return mCategory;
    }

    public void setmCategory(String mCategory) {
        this.mCategory = mCategory;
    }

    public String getmType() {
        return mType;
    }

    public void setmType(String mType) {
        this.mType = mType;
    }

    public double getmHandling() {
        return mHandling;
    }

    public void setmHandling(double mHandling) {
        this.mHandling = mHandling;
    }

    public boolean ismFireModeBurst() {
        return mFireModeBurst;
    }

    public void setmFireModeBurst(boolean mFireModeBurst) {
        this.mFireModeBurst = mFireModeBurst;
    }

    public double getmDamage() {
        return mDamage;
    }

    public void setmDamage(double mDamage) {
        this.mDamage = mDamage;
    }

    public double getmMobility() {
        return mMobility;
    }

    public void setmMobility(double mMobility) {
        this.mMobility = mMobility;
    }

    public boolean ismFireModeSingle() {
        return mFireModeSingle;
    }

    public void setmFireModeSingle(boolean mFireModeSingle) {
        this.mFireModeSingle = mFireModeSingle;
    }

    public String getmDesscription() {
        return mDesscription;
    }

    public void setmDesscription(String mDesscription) {
        this.mDesscription = mDesscription;
    }

    public boolean ismFireModeAuto() {
        return mFireModeAuto;
    }

    public void setmFireModeAuto(boolean mFireModeAuto) {
        this.mFireModeAuto = mFireModeAuto;
    }

    public double getmStatRange() {
        return mStatRange;
    }

    public void setmStatRange(double mStatRange) {
        this.mStatRange = mStatRange;
    }

    public String getmRange() {
        return mRange;
    }

    public void setmRange(String mRange) {
        this.mRange = mRange;
    }

    public String getmRateOfFire() {
        return mRateOfFire;
    }

    public void setmRateOfFire(String mRateOfFire) {
        this.mRateOfFire = mRateOfFire;
    }

    public String getmAmmoType() {
        return mAmmoType;
    }

    public void setmAmmoType(String mAmmoType) {
        this.mAmmoType = mAmmoType;
    }

    public Double getmWeaponAccuracy() {
        return mWeaponAccuracy;
    }

    public void setmWeaponAccuracy(Double mWeaponAccuracy) {
        this.mWeaponAccuracy = mWeaponAccuracy;
    }

    public String getmAmmo() {
        return mAmmo;
    }

    public void setmAmmo(String mAmmo) {
        this.mAmmo = mAmmo;
    }

    public String getmUnlockLicense() {
        return mUnlockLicense;
    }

    public void setmUnlockLicense(String mUnlockLicense) {
        this.mUnlockLicense = mUnlockLicense;
    }

    public String getmUnlockCode() {
        return mUnlockCode;
    }

    public void setmUnlockCode(String mUnlockCode) {
        this.mUnlockCode = mUnlockCode;
    }

    public String getmUnlockName() {
        return mUnlockName;
    }

    public void setmUnlockName(String mUnlockName) {
        this.mUnlockName = mUnlockName;
    }

    public int getmUnlockNeededl() {
        return mUnlockNeededl;
    }

    public void setmUnlockNeededl(int mUnlockNeededl) {
        this.mUnlockNeededl = mUnlockNeededl;
    }

    public long getmKillPerMinute() {
        return mKillPerMinute;
    }

    public void setmKillPerMinute(long mKillPerMinute) {
        this.mKillPerMinute = mKillPerMinute;
    }

    public double getmSFPM() {
        return mSFPM;
    }

    public void setmSFPM(double mSFPM) {
        this.mSFPM = mSFPM;
    }

    public double getmHeadKillPercentage() {
        return mHeadKillPercentage;
    }

    public void setmHeadKillPercentage(double mHeadKillPercentage) {
        this.mHeadKillPercentage = mHeadKillPercentage;
    }

    public double getmAccuracy() {
        return mAccuracy;
    }

    public void setmAccuracy(double mAccuracy) {
        this.mAccuracy = mAccuracy;
    }

    public String getmImageFancy() {
        return mImageFancy;
    }

    public void setmImageFancy(String mImageFancy) {
        this.mImageFancy = mImageFancy;
    }

    public String getmImageLineart() {
        return mImageLineart;
    }

    public void setmImageLineart(String mImageLineart) {
        this.mImageLineart = mImageLineart;
    }

    @Override
    public int compareTo(Weapons another) {
        int compareQuantity = ((Weapons) another).getmKills();
        return compareQuantity - this.getmKills();
    }
}
