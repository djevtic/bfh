package com.bfh.djevtic.statisticsforbfh.models;

/**
 * Created by djevtic on 1.4.2015.
 */
public class Modes {

    private String mId;
    private int mScore;
    private String mName;

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public int getmScore() {
        return mScore;
    }

    public void setmScore(int mScore) {
        this.mScore = mScore;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }


}
