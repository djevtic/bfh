package com.bfh.djevtic.statisticsforbfh.models;

import java.util.ArrayList;

/**
 * Created by djevtic on 3.4.2015.
 */
public class Statistics {
    private double mKDRating;
    private double mAccuracy;
    private long mSkill;
    private long mElo;
    private long mRank;
    private long mTimePlayed;
    private long mKills;
    private long mDeaths;
    private long mHeadshots;
    private long mShootsFired;
    private long mShootsHit;
    private long mSuppressionAssist;
    private long mAvengerKills;
    private long mSaviourKills;
    private long mNemesisKills;
    private int mNumberOfRounds;
    private int mNumberofLosses;
    private int mNumberOfWins;
    private int mKillStreakBonus;
    private int mNemesisStreak;
    private int mcomDefendKills;
    private int mResuplies;
    private int mRepaires;
    private int mHeals;
    private int mRevives;
    private long mLongestHeadshot;
    private int mLongestWinStreak;
    private int mFlagDefend;
    private int mFlagCaptures;
    private int mKillAssists;
    private int mVehicleDestroyed;
    private int mVehicleDamaged;
    private int mDogtagsTaken;
    private int mStreak;
    private int mBestStreak;

    //Compleh data
    private Scores mScores;
    private Resets mResets;
    private ArrayList<Modes> mModes;
    private ArrayList<Kits> mKits;
    private ExtrasScore mExtra;

    public double getmKDRating() {
        return mKDRating;
    }

    public void setmKDRating(double mKDRating) {
        this.mKDRating = mKDRating;
    }

    public double getmAccuracy() {
        return mAccuracy;
    }

    public void setmAccuracy(double mAccuracy) {
        this.mAccuracy = mAccuracy;
    }

    public long getmSkill() {
        return mSkill;
    }

    public void setmSkill(long mSkill) {
        this.mSkill = mSkill;
    }

    public long getmElo() {
        return mElo;
    }

    public void setmElo(long mElo) {
        this.mElo = mElo;
    }

    public long getmRank() {
        return mRank;
    }

    public void setmRank(long mRank) {
        this.mRank = mRank;
    }

    public long getmTimePlayed() {
        return mTimePlayed;
    }

    public void setmTimePlayed(long mTimePlayed) {
        this.mTimePlayed = mTimePlayed;
    }

    public long getmKills() {
        return mKills;
    }

    public void setmKills(long mKills) {
        this.mKills = mKills;
    }

    public long getmDeaths() {
        return mDeaths;
    }

    public void setmDeaths(long mDeaths) {
        this.mDeaths = mDeaths;
    }

    public long getmHeadshots() {
        return mHeadshots;
    }

    public void setmHeadshots(long mHeadshots) {
        this.mHeadshots = mHeadshots;
    }

    public long getmShootsFired() {
        return mShootsFired;
    }

    public void setmShootsFired(long mShootsFired) {
        this.mShootsFired = mShootsFired;
    }

    public long getmShootsHit() {
        return mShootsHit;
    }

    public void setmShootsHit(long mShootsHit) {
        this.mShootsHit = mShootsHit;
    }

    public long getmSuppressionAssist() {
        return mSuppressionAssist;
    }

    public void setmSuppressionAssist(long mSuppressionAssist) {
        this.mSuppressionAssist = mSuppressionAssist;
    }

    public long getmAvengerKills() {
        return mAvengerKills;
    }

    public void setmAvengerKills(long mAvengerKills) {
        this.mAvengerKills = mAvengerKills;
    }

    public long getmSaviourKills() {
        return mSaviourKills;
    }

    public void setmSaviourKills(long mSaviourKills) {
        this.mSaviourKills = mSaviourKills;
    }

    public long getmNemesisKills() {
        return mNemesisKills;
    }

    public void setmNemesisKills(long mNemesisKills) {
        this.mNemesisKills = mNemesisKills;
    }

    public int getmNumberOfRounds() {
        return mNumberOfRounds;
    }

    public void setmNumberOfRounds(int mNumberOfRounds) {
        this.mNumberOfRounds = mNumberOfRounds;
    }

    public int getmNumberofLosses() {
        return mNumberofLosses;
    }

    public void setmNumberofLosses(int mNumberofLosses) {
        this.mNumberofLosses = mNumberofLosses;
    }

    public int getmNumberOfWins() {
        return mNumberOfWins;
    }

    public void setmNumberOfWins(int mNumberOfWins) {
        this.mNumberOfWins = mNumberOfWins;
    }

    public int getmKillStreakBonus() {
        return mKillStreakBonus;
    }

    public void setmKillStreakBonus(int mKillStreakBonus) {
        this.mKillStreakBonus = mKillStreakBonus;
    }

    public int getmNemesisStreak() {
        return mNemesisStreak;
    }

    public void setmNemesisStreak(int mNemesisStreak) {
        this.mNemesisStreak = mNemesisStreak;
    }

    public int getMcomDefendKills() {
        return mcomDefendKills;
    }

    public void setMcomDefendKills(int mcomDefendKills) {
        this.mcomDefendKills = mcomDefendKills;
    }

    public int getmResuplies() {
        return mResuplies;
    }

    public void setmResuplies(int mResuplies) {
        this.mResuplies = mResuplies;
    }

    public int getmRepaires() {
        return mRepaires;
    }

    public void setmRepaires(int mRepaires) {
        this.mRepaires = mRepaires;
    }

    public int getmHeals() {
        return mHeals;
    }

    public void setmHeals(int mHeals) {
        this.mHeals = mHeals;
    }

    public int getmRevives() {
        return mRevives;
    }

    public void setmRevives(int mRevives) {
        this.mRevives = mRevives;
    }

    public long getmLongestHeadshot() {
        return mLongestHeadshot;
    }

    public void setmLongestHeadshot(long mLongestHeadshot) {
        this.mLongestHeadshot = mLongestHeadshot;
    }

    public int getmLongestWinStreak() {
        return mLongestWinStreak;
    }

    public void setmLongestWinStreak(int mLongestWinStreak) {
        this.mLongestWinStreak = mLongestWinStreak;
    }

    public int getmFlagDefend() {
        return mFlagDefend;
    }

    public void setmFlagDefend(int mFlagDefend) {
        this.mFlagDefend = mFlagDefend;
    }

    public int getmFlagCaptures() {
        return mFlagCaptures;
    }

    public void setmFlagCaptures(int mFlagCaptures) {
        this.mFlagCaptures = mFlagCaptures;
    }

    public int getmKillAssists() {
        return mKillAssists;
    }

    public void setmKillAssists(int mKillAssists) {
        this.mKillAssists = mKillAssists;
    }

    public int getmVehicleDestroyed() {
        return mVehicleDestroyed;
    }

    public void setmVehicleDestroyed(int mVehicleDestroyed) {
        this.mVehicleDestroyed = mVehicleDestroyed;
    }

    public int getmVehicleDamaged() {
        return mVehicleDamaged;
    }

    public void setmVehicleDamaged(int mVehicleDamaged) {
        this.mVehicleDamaged = mVehicleDamaged;
    }

    public int getmDogtagsTaken() {
        return mDogtagsTaken;
    }

    public void setmDogtagsTaken(int mDogtagsTaken) {
        this.mDogtagsTaken = mDogtagsTaken;
    }

    public int getmStreak() {
        return mStreak;
    }

    public void setmStreak(int mStreak) {
        this.mStreak = mStreak;
    }

    public int getmBestStreak() {
        return mBestStreak;
    }

    public void setmBestStreak(int mBestStreak) {
        this.mBestStreak = mBestStreak;
    }

    public Scores getmScores() {
        return mScores;
    }

    public void setmScores(Scores mScores) {
        this.mScores = mScores;
    }

    public Resets getmResets() {
        return mResets;
    }

    public void setmResets(Resets mResets) {
        this.mResets = mResets;
    }

    public ArrayList<Modes> getmModes() {
        return mModes;
    }

    public void setmModes(ArrayList<Modes> mModes) {
        this.mModes = mModes;
    }

    public ArrayList<Kits> getmKits() {
        return mKits;
    }

    public void setmKits(ArrayList<Kits> mKits) {
        this.mKits = mKits;
    }

    public ExtrasScore getmExtra() {
        return mExtra;
    }

    public void setmExtra(ExtrasScore mExtra) {
        this.mExtra = mExtra;
    }
}
