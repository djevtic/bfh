package com.bfh.djevtic.statisticsforbfh.fragments.separatefragments;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bfh.djevtic.statisticsforbfh.R;
import com.bfh.djevtic.statisticsforbfh.background.adapters.WeaponCategoryListAdapter;
import com.bfh.djevtic.statisticsforbfh.background.adapters.WeaponListAdapter;
import com.bfh.djevtic.statisticsforbfh.globals.Globals;
import com.bfh.djevtic.statisticsforbfh.models.WeaponCategory;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by djevt_000 on 4/14/2015.
 */
public class WeaponCategoryFragmentList  extends ListFragment {
    private ArrayList<WeaponCategory> mWeaponCategory;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mWeaponCategory = Globals.player.getmWeaponCategoryList();
        Collections.sort(mWeaponCategory);
        setListAdapter(new WeaponCategoryListAdapter(getActivity().getApplicationContext(), R.layout.modes_list_item_layout, mWeaponCategory));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mode_list_fragment, null, false);
        AdView mAdView = (AdView) view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        return view;
    }
}
