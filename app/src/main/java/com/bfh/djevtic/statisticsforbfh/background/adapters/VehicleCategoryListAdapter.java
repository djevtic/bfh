package com.bfh.djevtic.statisticsforbfh.background.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bfh.djevtic.statisticsforbfh.R;
import com.bfh.djevtic.statisticsforbfh.globals.Globals;
import com.bfh.djevtic.statisticsforbfh.models.VehicleCategory;
import com.bfh.djevtic.statisticsforbfh.models.Vehicles;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by djevt_000 on 4/14/2015.
 */
public class VehicleCategoryListAdapter extends ArrayAdapter {

    private Context mContext;
    private ArrayList<VehicleCategory> mVehicleCategory;

    private static final String TAG = "djevtic";

    public VehicleCategoryListAdapter(Context context, int resource, ArrayList<VehicleCategory> vehicles) {
        super(context, resource, vehicles);
        this.mContext = context;
        this.mVehicleCategory = vehicles;
    }

    private static class ViewHolder {
        TextView name;
        TextView kills;
        TextView destroys;
        TextView time;
        TextView kpm;
        TextView spm;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        VehicleCategory vehicles = mVehicleCategory.get(position);
        if(convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.vehicle_category_list_item, null);
            }

            viewHolder = new ViewHolder();

            viewHolder.name = (TextView) convertView.findViewById(R.id.vehicle_category_name);
            viewHolder.kills = (TextView) convertView.findViewById(R.id.vehicle_category_kills);
            viewHolder.destroys = (TextView) convertView.findViewById(R.id.vehicle_category_destroy);
            viewHolder.kpm = (TextView) convertView.findViewById(R.id.vehicle_category_kpm);
            viewHolder.time = (TextView) convertView.findViewById(R.id.vehicle_category_time);
            viewHolder.spm = (TextView) convertView.findViewById(R.id.vehicle_category_spm);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.name.setText(String.valueOf(vehicles.getmName()));
        viewHolder.kills.setText(String.valueOf(vehicles.getmKills()));
        viewHolder.destroys.setText(String.valueOf(vehicles.getmDestroys()));
        viewHolder.time.setText(Globals.getTime(vehicles.getmTime()));
        viewHolder.kpm.setText(String.valueOf(vehicles.getmKPM()));
        viewHolder.spm.setText(String.valueOf(vehicles.getmSPM()));
        return convertView;
    }
}
