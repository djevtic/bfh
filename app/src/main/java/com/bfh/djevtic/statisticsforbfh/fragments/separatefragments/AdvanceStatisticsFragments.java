package com.bfh.djevtic.statisticsforbfh.fragments.separatefragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.bfh.djevtic.statisticsforbfh.R;
import com.bfh.djevtic.statisticsforbfh.globals.Globals;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

/**
 * Created by djevtic on 7.4.2015.
 */
public class AdvanceStatisticsFragments extends Fragment {

    @Override
    public View onCreateView(LayoutInflater inflater,
                             ViewGroup container, Bundle savedInstanceState) {
        View rootView = inflater.inflate(
                R.layout.fragment_advanced_statistics_layout, container, false);

        AdView mAdView = (AdView) rootView.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);

        TextView kd = (TextView) rootView.findViewById(R.id.score_kd);
        TextView accuracy = (TextView) rootView.findViewById(R.id.score_accuracy);
        TextView skill = (TextView) rootView.findViewById(R.id.score_skill);
        TextView elo = (TextView) rootView.findViewById(R.id.score_elo);
        TextView timePlayed = (TextView) rootView.findViewById(R.id.score_time_played);
        TextView kills = (TextView) rootView.findViewById(R.id.score_kills);
        TextView deaths = (TextView) rootView.findViewById(R.id.score_deaths);
        TextView headshots = (TextView) rootView.findViewById(R.id.score_headshots);
        TextView shotsFired = (TextView) rootView.findViewById(R.id.score_shots_fired);
        TextView shotsHit = (TextView) rootView.findViewById(R.id.score_shots_hit);
        TextView supresionAssist = (TextView) rootView.findViewById(R.id.score_suppression_assist);
        TextView avengerKills = (TextView) rootView.findViewById(R.id.score_avenger_kills);
        TextView saviorKills = (TextView) rootView.findViewById(R.id.score_savior_kills);
        TextView nemesisKills = (TextView) rootView.findViewById(R.id.score_nemesis_kills);
        TextView numberOfRounds = (TextView) rootView.findViewById(R.id.score_number_rounds_played);
        TextView numberOfLoses = (TextView) rootView.findViewById(R.id.score_number_rounds_lost);
        TextView numberOfWins = (TextView) rootView.findViewById(R.id.score_number_rounds_win);
        TextView killStreakBonus = (TextView) rootView.findViewById(R.id.score_kill_streak_bonus);
        TextView nemesisStreak = (TextView) rootView.findViewById(R.id.score_nemesis_streak);
        TextView mcomDefenseKills = (TextView) rootView.findViewById(R.id.score_mcom_defend_kills);
        TextView resuplies = (TextView) rootView.findViewById(R.id.score_resuplies);
        TextView repairs = (TextView) rootView.findViewById(R.id.score_repairs);
        TextView heals = (TextView) rootView.findViewById(R.id.score_heals);
        TextView revives = (TextView) rootView.findViewById(R.id.score_revives);
        TextView longestHeadshot = (TextView) rootView.findViewById(R.id.score_longest_headshot);
        TextView longestWinStreak = (TextView) rootView.findViewById(R.id.score_longest_win_streak);
        TextView flagDefend = (TextView) rootView.findViewById(R.id.score_flag_defend);
        TextView flagCapture = (TextView) rootView.findViewById(R.id.score_flag_capture);
        TextView killAssist = (TextView) rootView.findViewById(R.id.score_kill_assist);
        TextView vehicleDestroyed = (TextView) rootView.findViewById(R.id.score_vehicel_destroyed);
        TextView vehicleDamaged = (TextView) rootView.findViewById(R.id.score_vehicel_damaged);
        TextView dogtagsTaken = (TextView) rootView.findViewById(R.id.score_dogtags_taken);
        TextView streak = (TextView) rootView.findViewById(R.id.score_streak);
        TextView bestStreak = (TextView) rootView.findViewById(R.id.score_best_streak);

        if(Globals.player.getmStatistics()!= null) {
            kd.setText(String.valueOf(Globals.player.getmStatistics().getmKDRating()));
            accuracy.setText(String.valueOf(Globals.player.getmStatistics().getmAccuracy()));
            skill.setText(String.valueOf(Globals.player.getmStatistics().getmSkill()));
            elo.setText(String.valueOf(Globals.player.getmStatistics().getmElo()));
            timePlayed.setText(Globals.getTime(Globals.player.getmStatistics().getmTimePlayed()));
            kills.setText(String.valueOf(Globals.player.getmStatistics().getmKills()));
            deaths.setText(String.valueOf(Globals.player.getmStatistics().getmDeaths()));
            headshots.setText(String.valueOf(Globals.player.getmStatistics().getmHeadshots()));
            shotsFired.setText(String.valueOf(Globals.player.getmStatistics().getmShootsFired()));
            shotsHit.setText(String.valueOf(Globals.player.getmStatistics().getmShootsHit()));
            supresionAssist.setText(String.valueOf(Globals.player.getmStatistics().getmSuppressionAssist()));
            avengerKills.setText(String.valueOf(Globals.player.getmStatistics().getmAvengerKills()));
            saviorKills.setText(String.valueOf(Globals.player.getmStatistics().getmSaviourKills()));
            nemesisKills.setText(String.valueOf(Globals.player.getmStatistics().getmNemesisKills()));
            numberOfRounds.setText(String.valueOf(Globals.player.getmStatistics().getmNumberOfRounds()));
            numberOfLoses.setText(String.valueOf(Globals.player.getmStatistics().getmNumberofLosses()));
            numberOfWins.setText(String.valueOf(Globals.player.getmStatistics().getmNumberOfWins()));
            killStreakBonus.setText(String.valueOf(Globals.player.getmStatistics().getmKillStreakBonus()));
            nemesisStreak.setText(String.valueOf(Globals.player.getmStatistics().getmNemesisStreak()));
            mcomDefenseKills.setText(String.valueOf(Globals.player.getmStatistics().getMcomDefendKills()));
            resuplies.setText(String.valueOf(Globals.player.getmStatistics().getmResuplies()));
            repairs.setText(String.valueOf(Globals.player.getmStatistics().getmRepaires()));
            heals.setText(String.valueOf(Globals.player.getmStatistics().getmHeals()));
            revives.setText(String.valueOf(Globals.player.getmStatistics().getmRevives()));
            longestHeadshot.setText(String.valueOf(Globals.player.getmStatistics().getmLongestHeadshot()));
            longestWinStreak.setText(String.valueOf(Globals.player.getmStatistics().getmLongestWinStreak()));
            flagDefend.setText(String.valueOf(Globals.player.getmStatistics().getmFlagDefend()));
            flagCapture.setText(String.valueOf(Globals.player.getmStatistics().getmFlagCaptures()));
            killAssist.setText(String.valueOf(Globals.player.getmStatistics().getmKillAssists()));
            vehicleDestroyed.setText(String.valueOf(Globals.player.getmStatistics().getmVehicleDestroyed()));
            vehicleDamaged.setText(String.valueOf(Globals.player.getmStatistics().getmVehicleDamaged()));
            dogtagsTaken.setText(String.valueOf(Globals.player.getmStatistics().getmDogtagsTaken()));
            streak.setText(String.valueOf(Globals.player.getmStatistics().getmStreak()));
            bestStreak.setText(String.valueOf(Globals.player.getmStatistics().getmBestStreak()));
        }
        return rootView;
    }
}
