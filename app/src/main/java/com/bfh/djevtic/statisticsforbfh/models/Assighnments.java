package com.bfh.djevtic.statisticsforbfh.models;

import java.util.ArrayList;

/**
 * Created by djevtic on 1.4.2015.
 */
public class Assighnments {
    private String mID;
    private String mName;
    private String mCategory;
    private String mDescription;
    private ArrayList<AssighnmentDependencies> mDependencies;
    private ArrayList<AssighnmentCriterias> mCriterias;
    private int mCurrent;
    private int mNeeded;
    private int mProg;
    private String mImage;

    public String getmID() {
        return mID;
    }

    public void setmID(String mID) {
        this.mID = mID;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmCategory() {
        return mCategory;
    }

    public void setmCategory(String mCategory) {
        this.mCategory = mCategory;
    }

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public ArrayList<AssighnmentDependencies> getmDependencies() {
        return mDependencies;
    }

    public void setmDependencies(ArrayList<AssighnmentDependencies> mDependencies) {
        this.mDependencies = mDependencies;
    }

    public ArrayList<AssighnmentCriterias> getmCriterias() {
        return mCriterias;
    }

    public void setmCriterias(ArrayList<AssighnmentCriterias> mCriterias) {
        this.mCriterias = mCriterias;
    }

    public int getmCurrent() {
        return mCurrent;
    }

    public void setmCurrent(int mCurrent) {
        this.mCurrent = mCurrent;
    }

    public int getmNeeded() {
        return mNeeded;
    }

    public void setmNeeded(int mNeeded) {
        this.mNeeded = mNeeded;
    }

    public int getmProg() {
        return mProg;
    }

    public void setmProg(int mProg) {
        this.mProg = mProg;
    }

    public String getmImage() {
        return mImage;
    }

    public void setmImage(String mImage) {
        this.mImage = mImage;
    }
}