package com.bfh.djevtic.statisticsforbfh.fragments;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.bfh.djevtic.statisticsforbfh.fragments.separatefragments.AdvanceStatisticsFragments;
import com.bfh.djevtic.statisticsforbfh.fragments.separatefragments.AwardsGridViewFragment;
import com.bfh.djevtic.statisticsforbfh.fragments.separatefragments.KitFragmentList;
import com.bfh.djevtic.statisticsforbfh.fragments.separatefragments.ModesFragmentList;
import com.bfh.djevtic.statisticsforbfh.fragments.separatefragments.StatisticsFragment;
import com.bfh.djevtic.statisticsforbfh.fragments.separatefragments.UpcomingUnlocksFragmentList;
import com.bfh.djevtic.statisticsforbfh.fragments.separatefragments.VehicleCategoryFragmentList;
import com.bfh.djevtic.statisticsforbfh.fragments.separatefragments.VehicleFragmentList;
import com.bfh.djevtic.statisticsforbfh.fragments.separatefragments.WeaponCategoryFragmentList;
import com.bfh.djevtic.statisticsforbfh.fragments.separatefragments.WeaponFragmentList;

/**
 * Created by djevt_000 on 4/6/2015.
 */
public class CollectionPageAdapter extends FragmentStatePagerAdapter{

    public CollectionPageAdapter(FragmentManager fragmentManager){
        super(fragmentManager);
    }
    @Override
    public Fragment getItem(int itemPosition) {
        Fragment fragment;
        if(itemPosition == 0) {
            fragment = new StatisticsFragment();
        }else if(itemPosition == 1){
            fragment = new AdvanceStatisticsFragments();
        }else if(itemPosition == 2){
            fragment = new ModesFragmentList();
        }else if(itemPosition == 3){
            fragment = new KitFragmentList();
        }else if(itemPosition == 4){
            fragment = new WeaponFragmentList();
        }else if(itemPosition == 5){
            fragment = new WeaponCategoryFragmentList();
        }else if(itemPosition == 6){
            fragment = new VehicleFragmentList();
        }else if(itemPosition == 7){
            fragment = new VehicleCategoryFragmentList();
        }else if(itemPosition == 8){
            fragment = new AwardsGridViewFragment();
        }
        else if(itemPosition == 9){
            fragment = new UpcomingUnlocksFragmentList();
        }else {
            fragment = new AdvanceStatisticsFragments();
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 10;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        CharSequence pageName;
        if(position == 0) {
            pageName = "Statistics";
        }else if(position == 1){
            pageName = "Advanced Statistics";
        }else if(position == 2){
            pageName = "Mods";
        }else if(position == 3){
            pageName = "Kits";
        }else if(position == 4){
            pageName = "Weapons";
        }else if(position == 5){
            pageName = "Weapons Category";
        }else if(position == 6){
            pageName = "Vehicles";
        }else if(position == 7){
            pageName = "Vehicles Category";
        }else if(position == 8){
            pageName = "Awards";
        }else if(position == 9){
            pageName = "Unlocks";
        }else{
            pageName = "UPS";
        }
        return pageName;
    }
}
