package com.bfh.djevtic.statisticsforbfh.background.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bfh.djevtic.statisticsforbfh.R;
import com.bfh.djevtic.statisticsforbfh.globals.Globals;
import com.bfh.djevtic.statisticsforbfh.models.Weapons;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by djevtic on 9.4.2015.
 */
public class WeaponListAdapter extends ArrayAdapter {

    private Context mContext;
    private ArrayList<Weapons> mWeapons;

    public WeaponListAdapter(Context context, int resource, ArrayList<Weapons> weapons) {
        super(context, resource, weapons);
        this.mContext = context;
        this.mWeapons = weapons;
    }

    private static class ViewHolder {
        TextView name;
        ImageView img;
        TextView kills;
        TextView headshots;
        TextView shots;
        TextView hits;
        TextView time;
        TextView bStars;
        TextView sStars;
        TextView gStars;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        Weapons weapons = mWeapons.get(position);
        if(convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.weapon_list_item_layout, null);
            }

            viewHolder = new ViewHolder();

            viewHolder.name = (TextView) convertView.findViewById(R.id.weapon_list_item_name);
            viewHolder.img = (ImageView) convertView.findViewById(R.id.weapon_image);
            viewHolder.kills = (TextView) convertView.findViewById(R.id.weapon_kills);
            viewHolder.headshots = (TextView) convertView.findViewById(R.id.weapon_headshots);
            viewHolder.shots = (TextView) convertView.findViewById(R.id.weapon_shots);
            viewHolder.hits = (TextView) convertView.findViewById(R.id.weapon_hits);
            viewHolder.time = (TextView) convertView.findViewById(R.id.weapon_time);
            viewHolder.bStars = (TextView) convertView.findViewById(R.id.weapon_bronze_star_count);
            viewHolder.sStars = (TextView) convertView.findViewById(R.id.weapon_silver_star_count);
            viewHolder.gStars = (TextView) convertView.findViewById(R.id.weapon_gold_star_count);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.name.setText(String.valueOf(weapons.getmName()));
        Picasso.with(mContext).load(mContext.getString(R.string.dropbox_url) + weapons.getmImageFancy()).placeholder(R.drawable.maintenance).into(viewHolder.img);
        viewHolder.kills.setText(String.valueOf(weapons.getmKills()));
        viewHolder.headshots.setText(String.valueOf(weapons.getmHeadShots()));
        viewHolder.shots.setText(String.valueOf(weapons.getmShoots()));
        viewHolder.hits.setText(String.valueOf(weapons.getmHits()));
        viewHolder.time.setText(Globals.getTime(weapons.getmTime()));
        viewHolder.bStars.setText(String.valueOf(weapons.getmBStart()));
        viewHolder.sStars.setText(String.valueOf(weapons.getmSStars()));
        viewHolder.gStars.setText(String.valueOf(weapons.getmGStars()                                                                                                                        ));
        return convertView;
    }
}
