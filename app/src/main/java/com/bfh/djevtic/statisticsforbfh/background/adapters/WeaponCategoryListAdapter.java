package com.bfh.djevtic.statisticsforbfh.background.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bfh.djevtic.statisticsforbfh.R;
import com.bfh.djevtic.statisticsforbfh.globals.Globals;
import com.bfh.djevtic.statisticsforbfh.models.WeaponCategory;
import com.bfh.djevtic.statisticsforbfh.models.Weapons;

import java.util.ArrayList;

/**
 * Created by djevt_000 on 4/14/2015.
 */
public class WeaponCategoryListAdapter extends ArrayAdapter {

    public static final String URL = "https://dl.dropboxusercontent.com/u/1480498/";

    private Context mContext;
    private ArrayList<WeaponCategory> mCategory;

    private static final String TAG = "djevtic";

    public WeaponCategoryListAdapter(Context context, int resource, ArrayList<WeaponCategory> wCategory) {
        super(context, resource, wCategory);
        this.mContext = context;
        this.mCategory = wCategory;
    }

    private static class ViewHolder {
        TextView name;
        TextView time;
        TextView kills;
        TextView hs;
        TextView hits;
        TextView shots;
        TextView kpm;
        TextView spm;
        TextView sfpm;
        TextView hkp;
        TextView accuracy;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d(TAG, "ModesListAdapter: getView");
        ViewHolder viewHolder;
        WeaponCategory category = mCategory.get(position);
        if (convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.weapon_category_item_layout, null);
            }

            viewHolder = new ViewHolder();

            viewHolder.name = (TextView) convertView.findViewById(R.id.weapon_category_name);
            viewHolder.time = (TextView) convertView.findViewById(R.id.weapon_category_time);
            viewHolder.kills = (TextView) convertView.findViewById(R.id.weapon_category_kills);
            viewHolder.hs = (TextView) convertView.findViewById(R.id.weapon_category_headshots);
            viewHolder.hits = (TextView) convertView.findViewById(R.id.weapon_category_hits);
            viewHolder.shots = (TextView) convertView.findViewById(R.id.weapon_category_shots);
            viewHolder.kpm = (TextView) convertView.findViewById(R.id.weapon_category_kpm);
            viewHolder.spm = (TextView) convertView.findViewById(R.id.weapon_category_spm);
            viewHolder.sfpm = (TextView) convertView.findViewById(R.id.weapon_category_sfpm);
            viewHolder.hkp = (TextView) convertView.findViewById(R.id.weapon_category_hkp);
            viewHolder.accuracy = (TextView) convertView.findViewById(R.id.weapon_category_accuracy);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.name.setText(category.getmName());
        viewHolder.time.setText(Globals.getTime(category.getmTime()));
        viewHolder.kills.setText(String.valueOf(category.getmKills()));
        viewHolder.hs.setText(String.valueOf(category.getmHeadShots()));
        viewHolder.hits.setText(String.valueOf(category.getmHits()));
        viewHolder.shots.setText(String.valueOf(category.getmShots()));
        viewHolder.kpm.setText(String.valueOf(category.getmKPM()));
        viewHolder.spm.setText(String.valueOf(category.getmSPM()));
        viewHolder.sfpm.setText(String.valueOf(category.getmSFPM()));
        viewHolder.hkp.setText(String.valueOf(category.getmHeadKillPercentuage()));
        viewHolder.accuracy.setText(String.valueOf(category.getmAccuracy()));

        return convertView;
    }
}
