package com.bfh.djevtic.statisticsforbfh.models;

/**
 * Created by djevtic on 1.4.2015.
 */
public class Awards   implements Comparable<Awards>{
    private String mId;
    private int mMedalCount;
    private int mRibonCount;
    private String mMedalName;
    private String mRibonName;

    private String mMedalDescription;
    private String mRibonDescription;

    private int mCurrentRibbons;
    private int mRibonsNeeded;

    private String mMedalImg;
    private String mRibbonImg;

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public int getmMedalCount() {
        return mMedalCount;
    }

    public void setmMedalCount(int mMedalCount) {
        this.mMedalCount = mMedalCount;
    }

    public int getmRibonCount() {
        return mRibonCount;
    }

    public void setmRibonCount(int mRibonCount) {
        this.mRibonCount = mRibonCount;
    }

    public String getmMedalName() {
        return mMedalName;
    }

    public void setmMedalName(String mMedalName) {
        this.mMedalName = mMedalName;
    }

    public String getmRibonName() {
        return mRibonName;
    }

    public void setmRibonName(String mRibonName) {
        this.mRibonName = mRibonName;
    }

    public String getmMedalDescription() {
        return mMedalDescription;
    }

    public void setmMedalDescription(String mMedalDescription) {
        this.mMedalDescription = mMedalDescription;
    }

    public String getmRibonDescription() {
        return mRibonDescription;
    }

    public void setmRibonDescription(String mRibonDescription) {
        this.mRibonDescription = mRibonDescription;
    }

    public int getmCurrentRibbons() {
        return mCurrentRibbons;
    }

    public void setmCurrentRibbons(int mCurrentRibbons) {
        this.mCurrentRibbons = mCurrentRibbons;
    }

    public int getmRibonsNeeded() {
        return mRibonsNeeded;
    }

    public void setmRibonsNeeded(int mRibonsNeeded) {
        this.mRibonsNeeded = mRibonsNeeded;
    }

    public String getmMedalImg() {
        return mMedalImg;
    }

    public void setmMedalImg(String mMedalImg) {
        this.mMedalImg = mMedalImg;
    }

    public String getmRibbonImg() {
        return mRibbonImg;
    }

    public void setmRibbonImg(String mRibbonImg) {
        this.mRibbonImg = mRibbonImg;
    }

    @Override
    public int compareTo(Awards another) {
        int compareQuantity = ((Awards) another).getmMedalCount();
        return compareQuantity - this.getmMedalCount();
    }
}