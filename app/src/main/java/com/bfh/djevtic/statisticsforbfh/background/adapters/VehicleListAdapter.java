package com.bfh.djevtic.statisticsforbfh.background.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bfh.djevtic.statisticsforbfh.R;
import com.bfh.djevtic.statisticsforbfh.globals.Globals;
import com.bfh.djevtic.statisticsforbfh.models.Vehicles;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by djevt_000 on 4/14/2015.
 */
public class VehicleListAdapter  extends ArrayAdapter {

    private Context mContext;
    private ArrayList<Vehicles> mVehicles;

    public VehicleListAdapter(Context context, int resource, ArrayList<Vehicles> vehicles) {
        super(context, resource, vehicles);
        this.mContext = context;
        this.mVehicles = vehicles;
    }

    private static class ViewHolder {
        TextView name;
        ImageView img;
        TextView kills;
        TextView destroys;
        TextView time;
        TextView kpm;
        TextView bStars;
        TextView sStars;
        TextView gStars;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        Vehicles vehicles = mVehicles.get(position);
        if(convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.vehicle_list_item_layout, null);
            }

            viewHolder = new ViewHolder();

            viewHolder.name = (TextView) convertView.findViewById(R.id.vehicle_name);
            viewHolder.img = (ImageView) convertView.findViewById(R.id.vehicle_image);
            viewHolder.kills = (TextView) convertView.findViewById(R.id.vehicle_kills);
            viewHolder.destroys = (TextView) convertView.findViewById(R.id.vehicle_destroyed);
            viewHolder.kpm = (TextView) convertView.findViewById(R.id.vehicle_kpm);
            viewHolder.time = (TextView) convertView.findViewById(R.id.vehicle_time);
            viewHolder.bStars = (TextView) convertView.findViewById(R.id.vehicle_bronze_star_count);
            viewHolder.sStars = (TextView) convertView.findViewById(R.id.vehicle_silver_star_count);
            viewHolder.gStars = (TextView) convertView.findViewById(R.id.vehicle_gold_star_count);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.name.setText(String.valueOf(vehicles.getmName()));
        Picasso.with(mContext).load(mContext.getString(R.string.dropbox_url) + vehicles.getmImgFancy()).placeholder(R.drawable.maintenance).into(viewHolder.img);
        viewHolder.kills.setText(String.valueOf(vehicles.getmKills()));
        viewHolder.destroys.setText(String.valueOf(vehicles.getmDestroys()));
        viewHolder.time.setText(Globals.getTime(vehicles.getmTime()));
        viewHolder.kpm.setText(String.valueOf(vehicles.getmKPM()));
        viewHolder.bStars.setText(String.valueOf(vehicles.getmBStars()));
        viewHolder.sStars.setText(String.valueOf(vehicles.getmSStars()));
        viewHolder.gStars.setText(String.valueOf(vehicles.getmGStars()));
        return convertView;
    }
}
