package com.bfh.djevtic.statisticsforbfh.background.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.bfh.djevtic.statisticsforbfh.R;
import com.bfh.djevtic.statisticsforbfh.models.Modes;

import java.util.ArrayList;

/**
 * Created by djevt_000 on 4/7/2015.
 */
public class ModesListAdapter extends ArrayAdapter {

    private Context mContext;
    private ArrayList<Modes> mModes;

    private static final String TAG = "djevtic";

    public ModesListAdapter(Context context, int resource, ArrayList<Modes> modes) {
        super(context, resource, modes);
        this.mContext = context;
        this.mModes = modes;
        Log.d(TAG, "ModesListAdapter: size: "+modes.size());
    }

    private static class ViewHolder {
        TextView name;
        TextView score;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d(TAG, "ModesListAdapter: getView");
        ViewHolder viewHolder;
        Modes mode = mModes.get(position);
        if(convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.modes_list_item_layout, null);
            }

            viewHolder = new ViewHolder();

            viewHolder.name = (TextView) convertView.findViewById(R.id.mode_list_item_name);
            viewHolder.score = (TextView) convertView.findViewById(R.id.mode_list_item_score);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        Log.d(TAG, "ModesListAdapter: mode.getmName(): "+mode.getmName()+" mode.getmScore():"+mode.getmScore());
        viewHolder.name.setText(String.valueOf(mode.getmName()));
        viewHolder.score.setText(String.valueOf(mode.getmScore()));
        return convertView;
    }
}
