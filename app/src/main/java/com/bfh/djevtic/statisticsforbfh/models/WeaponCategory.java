package com.bfh.djevtic.statisticsforbfh.models;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by djevtic on 1.4.2015.
 */
public class WeaponCategory  implements Comparable<WeaponCategory> {
    private String mName;
    private int mScore;
    private long mTime;
    private int mKills;
    private int mHeadShots;
    private int mHits;
    private int mShots;

    private double mKPM;
    private double mSPM;
    private double mSFPM; //Shots fired per minute
    private double mHeadKillPercentuage;
    private double mAccuracy;

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public int getmScore() {
        return mScore;
    }

    public void setmScore(int mScore) {
        this.mScore = mScore;
    }

    public long getmTime() {
        return mTime;
    }

    public void setmTime(long mTime) {
        this.mTime = mTime;
    }

    public int getmKills() {
        return mKills;
    }

    public void setmKills(int mKills) {
        this.mKills = mKills;
    }

    public int getmHeadShots() {
        return mHeadShots;
    }

    public void setmHeadShots(int mHeadShots) {
        this.mHeadShots = mHeadShots;
    }

    public int getmHits() {
        return mHits;
    }

    public void setmHits(int mHits) {
        this.mHits = mHits;
    }

    public int getmShots() {
        return mShots;
    }

    public void setmShots(int mShots) {
        this.mShots = mShots;
    }

    public double getmKPM() {
        BigDecimal bd = new BigDecimal(mKPM);
        bd = bd.setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public void setmKPM(double mKPM) {
        this.mKPM = mKPM;
    }

    public double getmSPM() {
        BigDecimal bd = new BigDecimal(mSPM);
        bd = bd.setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public void setmSPM(double mSPM) {
        this.mSPM = mSPM;
    }

    public double getmSFPM() {
        BigDecimal bd = new BigDecimal(mSFPM);
        bd = bd.setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public void setmSFPM(double mSFPM) {
        this.mSFPM = mSFPM;
    }

    public double getmHeadKillPercentuage() {
        BigDecimal bd = new BigDecimal(mHeadKillPercentuage);
        bd = bd.setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public void setmHeadKillPercentuage(double mHeadKillPercentuage) {
        this.mHeadKillPercentuage = mHeadKillPercentuage;
    }

    public double getmAccuracy() {
        BigDecimal bd = new BigDecimal(mAccuracy);
        bd = bd.setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public void setmAccuracy(double mAccuracy) {
        this.mAccuracy = mAccuracy;
    }

    @Override
    public int compareTo(WeaponCategory another) {
        int compareQuantity = ((WeaponCategory) another).getmKills();
        return compareQuantity - this.getmKills();
    }
}