package com.bfh.djevtic.statisticsforbfh.ui;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Spinner;

import com.bfh.djevtic.statisticsforbfh.R;
import com.bfh.djevtic.statisticsforbfh.background.PlayerFiller;
import com.bfh.djevtic.statisticsforbfh.fragments.MainFragmentActivity;
import com.bfh.djevtic.statisticsforbfh.globals.Globals;
import com.bfh.djevtic.statisticsforbfh.models.Player;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Array;

/**
 * Created by djevtic on 20.3.2015.
 */
public class PlayerSelect extends Activity{

    private String TAG = "djevtic";

    private Button mSubmitButton;
    private EditText mPlayerName;
    private Spinner mPlatformSpiner;
    private String mPlatformSelected = "PC";
    private String URL = "http://api.bfhstats.com/api/playerInfo?plat=";
    private String NAME = "&name=";
    private String OUTPUT = "&output=json";
    private String mUrl;
    private ProgressDialog mProgress;
    private String mTemp ="";
    private boolean mFileReadable = false;
    private boolean mPlayerAdded = false;
    private boolean mWebUpdateInProgress = true;
    private WebView mWebView;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.player_select_layout);
        mSubmitButton = (Button) findViewById(R.id.player_select_submit);
        mPlayerName = (EditText) findViewById(R.id.player_select_name);
        mPlatformSpiner = (Spinner) findViewById(R.id.player_select_platform_spiner);
        mProgress = new ProgressDialog(this);
        mProgress.setTitle("Loading");
        mProgress.setMessage("Wait while loading player data...");
        setListeners();
    }

    private void setListeners() {
        mPlatformSpiner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                mPlatformSelected = mPlatformSpiner.getItemAtPosition(position).toString();
            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });
        mSubmitButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                getFormData();
            }
        });
    }

    private void getFormData(){
        if(mPlayerName.getText().toString().length()>0){
            mUrl = URL+mPlatformSelected.toLowerCase()+NAME+mPlayerName.getText().toString().trim()+OUTPUT;
            mUrl = mUrl.replaceAll(" ", "%20");
            new DownloadFilesTask().execute();
        }
    }

    private class DownloadFilesTask extends AsyncTask<Void, Void, Player> {

        @Override
        protected void onPreExecute() {
            mProgress.show();
        }

        protected Player doInBackground(Void... params) {
            HttpResponse response = setServerResponse(mUrl);
            Player player = getDataFromResponse(response);
            if(player!=null) {
                updatePlayerinWebView(player);
            }
            while(mWebUpdateInProgress){

            }
            player = getDataFromResponse(response);
            if(player!=null) {
                addPlayerToList(player);
            }
            return player;
        }


        protected void onPostExecute(Player player) {
            if (mProgress.isShowing()) {
                mProgress.dismiss();
            }
            if(player != null) {
                Globals.player = player;
                Intent intent;
                intent = new Intent(getApplicationContext(), MainFragmentActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
            }else{
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        PlayerSelect.this);
                // set title
                alertDialogBuilder.setTitle(getString(R.string.player_select_alert_dialog_title));
                // set dialog message
                alertDialogBuilder
                        .setMessage(getString(R.string.player_select_alert_dialog_text))
                        .setCancelable(false)
                        .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });
                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();
                // show it
                alertDialog.show();
            }
        }
    }
    
    private void updatePlayerinWebView(final Player player){
        mWebView = new WebView(getApplicationContext());
        mWebView.setWebViewClient(new WebViewClient() {

            @Override
            public boolean shouldOverrideUrlLoading(WebView view, String urlNewString) {
                String URL = "http://bfhstats.com/"+player.getmPlatform()+player.getmName();
                Log.d(TAG,"URL for web loading: "+URL);
                view.loadUrl(URL);
                return true;
            }

            @Override
            public void onPageFinished(WebView view, String url) {
                Log.d(TAG, "Page finished loading");
                mWebUpdateInProgress = false;
            }
        });
    }

    private void addPlayerToList(Player player) {
        JSONArray playerList;
        read();
        if(mFileReadable){
            try {
                playerList = new JSONArray(mTemp);
                for(int i = 0;playerList.length()>i;i++) {
                    try {
                        JSONObject row = playerList.getJSONObject(i);
                        if(player.getmName().equals(row.getString("name"))){
                            row.put("name",player.getmName());
                            row.put("url", mUrl);
                            if(player.getmRank() == null){
                                row.put("rankUrl", null);
                            }else {
                                row.put("rankUrl", player.getmRank().getmImage());
                            }
                            mPlayerAdded = true;
                            break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if(!mPlayerAdded){
                    JSONObject playerJson = new JSONObject();
                    playerJson.put("name",player.getmName());
                    playerJson.put("url", mUrl);
                    if(player.getmRank() == null){
                        playerJson.put("rankUrl", null);
                    }else {
                        playerJson.put("rankUrl", player.getmRank().getmImage());
                    }
                    playerList.put(playerJson);
                }
                save(playerList.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            playerList = new JSONArray();
            JSONObject playerJson = new JSONObject();
            try {
                playerJson.put("name",player.getmName());
                playerJson.put("url", mUrl);
                if(player.getmRank() == null){
                    playerJson.put("rankUrl", null);
                }else {
                    playerJson.put("rankUrl", player.getmRank().getmImage());
                }
                playerList.put(playerJson);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            save(playerList.toString());
        }
    }

    private HttpResponse setServerResponse(String url) {
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);
        try {
            return client.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Player getDataFromResponse(HttpResponse response){
        if (response != null) {
            // Check for response status if all OK using response
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                HttpEntity entity = response.getEntity();
                try {
                    String data = EntityUtils.toString(entity);
                    JSONObject obj = new JSONObject(data);
                    PlayerFiller filler = new PlayerFiller(obj);
                    if(filler.getmPlayer().getmStatistics()!=null) {
                        return filler.getmPlayer();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                // In case response is incorrect use saved file if available
                //mJsonStore = fileUtils.readCachedText();
                Log.d(TAG,"Error response");
            }
        }
        return null;
    }

    public void save(String data){
        try {
            FileOutputStream fOut = openFileOutput(getString(R.string.player_list_file_name),MODE_WORLD_READABLE);
            fOut.write(data.getBytes());
            fOut.close();
            Log.d("djevtic", "Data saved ");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public void read(){
        try{
            FileInputStream fin = openFileInput(getString(R.string.player_list_file_name));
            int c;
            while( (c = fin.read()) != -1){
                mTemp = mTemp + Character.toString((char)c);
            }
            Log.d("djevtic", "Read data: "+ mTemp);
            mFileReadable = true;
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
