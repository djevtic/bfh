package com.bfh.djevtic.statisticsforbfh.models;

/**
 * Created by djevtic on 1.4.2015.
 */
public class UpcomingUnlocks {
    private String mType;
    private String mName;
    private String mSubName;
    private String mImage;
    private String mSubImage;
    private int mNeeded;
    private String mFormat;
    private String mNName;
    private int mCurr;
    private int mProg;
    private int mLeft;
    private String mDescription;

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public String getmType() {
        return mType;
    }

    public void setmType(String mType) {
        this.mType = mType;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName.substring(3);
    }

    public String getmSubName() {
        return mSubName;
    }

    public void setmSubName(String mSubName) {
        this.mSubName = mSubName;
    }

    public String getmImage() {
        return mImage;
    }

    public void setmImage(String mImage) {
        this.mImage = mImage;
    }

    public String getmSubImage() {
        return mSubImage;
    }

    public void setmSubImage(String mSubImage) {
        this.mSubImage = mSubImage;
    }

    public int getmNeeded() {
        return mNeeded;
    }

    public void setmNeeded(int mNeeded) {
        this.mNeeded = mNeeded;
    }

    public String getmFormat() {
        return mFormat;
    }

    public void setmFormat(String mFormat) {
        this.mFormat = mFormat;
    }

    public String getmNName() {
        return mNName;
    }

    public void setmNName(String mNName) {
        this.mNName = mNName;
    }

    public int getmCurr() {
        return mCurr;
    }

    public void setmCurr(int mCurr) {
        this.mCurr = mCurr;
    }

    public int getmProg() {
        return mProg;
    }

    public void setmProg(int mProg) {
        this.mProg = mProg;
    }

    public int getmLeft() {
        return mLeft;
    }

    public void setmLeft(int mLeft) {
        this.mLeft = mLeft;
    }
}