package com.bfh.djevtic.statisticsforbfh.background.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bfh.djevtic.statisticsforbfh.R;
import com.bfh.djevtic.statisticsforbfh.globals.Globals;
import com.bfh.djevtic.statisticsforbfh.models.Kits;
import com.bfh.djevtic.statisticsforbfh.models.Modes;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by djevtic on 9.4.2015.
 */
public class KitListAdapter  extends ArrayAdapter {
    private Context mContext;
    private ArrayList<Kits> mKits;

    private static final String TAG = "djevtic";

    public KitListAdapter(Context context, int resource, ArrayList<Kits> kits) {
        super(context, resource, kits);
        this.mContext = context;
        this.mKits = kits;
        Log.d(TAG, "ModesListAdapter: size: " + kits.size());
    }

    private static class ViewHolder {
        ImageView icon;
        TextView name;
        TextView score;
        TextView time;
        TextView spm;
        TextView stars;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d(TAG, "ModesListAdapter: getView");
        ViewHolder viewHolder;
        Kits kits = mKits.get(position);
        if(convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.kits_list_item_layout, null);
            }

            viewHolder = new ViewHolder();
            viewHolder.icon = (ImageView) convertView.findViewById(R.id.kit_list_item_icon);
            viewHolder.name = (TextView) convertView.findViewById(R.id.kit_list_item_name);
            viewHolder.score = (TextView) convertView.findViewById(R.id.kit_item_score);
            viewHolder.time = (TextView) convertView.findViewById(R.id.kit_item_time);
            viewHolder.spm = (TextView) convertView.findViewById(R.id.kit_item_spm);
            viewHolder.stars = (TextView) convertView.findViewById(R.id.kit_item_stars);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        String imgUrl = mContext.getString(R.string.dropbox_url);
        if(kits.getmName().equals("Operator")){
            imgUrl = imgUrl + "bfh/lbicons/KitAssault.png";
        }else if(kits.getmName().equals("Mechanic")){
            imgUrl = imgUrl + "bfh/lbicons/KitEngineer.png";
        }else if(kits.getmName().equals("Enforcer")){
            imgUrl = imgUrl + "bfh/lbicons/KitSupport.png";
        }else if(kits.getmName().equals("Professional")){
            imgUrl = imgUrl + "bfh/lbicons/KitRecon.png";
        }else if(kits.getmName().equals("Professional")){
            imgUrl = imgUrl + "bfh/lbicons/KitRecon.png";
        }else if(kits.getmName().equals("Hacker")){
            imgUrl = imgUrl + "bfh/lbicons/hacker.png";
        }
        Picasso.with(mContext).load(imgUrl).placeholder(R.drawable.maintenance).into(viewHolder.icon);
        viewHolder.name.setText(String.valueOf(kits.getmName()));
        viewHolder.score.setText(String.valueOf(kits.getmScore()));
        viewHolder.time.setText(Globals.getTime(kits.getmScore()));
        viewHolder.spm.setText(String.valueOf(kits.getmScore()));
        viewHolder.stars.setText(String.valueOf(kits.getmScore()));
        return convertView;
    }
}
