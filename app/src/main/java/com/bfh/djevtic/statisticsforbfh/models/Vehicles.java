package com.bfh.djevtic.statisticsforbfh.models;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by djevtic on 1.4.2015.
 */
public class Vehicles implements Comparable<Vehicles> {
    private String mId;
    private int mDestroys;
    private int mKills;
    private int mTime;
    private int mBStars;
    private int mSStars;
    private int mGStars;

    private String mName;
    private String mCategory;

    private double mKPM;

    private String mImgFancy;
    private String mImgLineart;

    public String getmId() {
        return mId;
    }

    public void setmId(String mId) {
        this.mId = mId;
    }

    public int getmDestroys() {
        return mDestroys;
    }

    public void setmDestroys(int mDestroys) {
        this.mDestroys = mDestroys;
    }

    public int getmKills() {
        return mKills;
    }

    public void setmKills(int mKills) {
        this.mKills = mKills;
    }

    public int getmTime() {
        return mTime;
    }

    public void setmTime(int mTime) {
        this.mTime = mTime;
    }

    public int getmBStars() {
        return mBStars;
    }

    public void setmBStars(int mBStars) {
        this.mBStars = mBStars;
    }

    public int getmSStars() {
        return mSStars;
    }

    public void setmSStars(int mSStars) {
        this.mSStars = mSStars;
    }

    public int getmGStars() {
        return mGStars;
    }

    public void setmGStars(int mGStars) {
        this.mGStars = mGStars;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmCategory() {
        return mCategory;
    }

    public void setmCategory(String mCategory) {
        this.mCategory = mCategory;
    }

    public double getmKPM() {
        BigDecimal bd = new BigDecimal(mKPM);
        bd = bd.setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public void setmKPM(double mKPM) {
        this.mKPM = mKPM;
    }

    public String getmImgFancy() {
        return mImgFancy;
    }

    public void setmImgFancy(String mImgFancy) {
        this.mImgFancy = mImgFancy;
    }

    public String getmImgLineart() {
        return mImgLineart;
    }

    public void setmImgLineart(String mImgLineart) {
        this.mImgLineart = mImgLineart;
    }

    @Override
    public int compareTo(Vehicles another) {
        int compareQuantity = ((Vehicles) another).getmKills();
        return compareQuantity - this.getmKills();
    }
}
