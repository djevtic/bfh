package com.bfh.djevtic.statisticsforbfh.fragments;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import android.util.Log;

import com.bfh.djevtic.statisticsforbfh.R;
import com.bfh.djevtic.statisticsforbfh.background.PlayerSelectList;
import com.bfh.djevtic.statisticsforbfh.ui.PlayerSelect;

/**
 * Created by djevt_000 on 4/6/2015.
 */
public class MainFragmentActivity extends FragmentActivity{

    CollectionPageAdapter mCollectionPageAdapter;
    ViewPager mViewPager;

    public void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.fragment_main_layout);
        mCollectionPageAdapter = new CollectionPageAdapter(getSupportFragmentManager());
        mViewPager = (ViewPager)findViewById(R.id.pager);
        mViewPager.setAdapter(mCollectionPageAdapter);
    }

    public void onBackPressed(){
        Log.d("djevtic", "Back pressed");
        Intent intent = new Intent(getApplicationContext(), PlayerSelectList.class);
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        getApplicationContext().startActivity(intent);
    }

}
