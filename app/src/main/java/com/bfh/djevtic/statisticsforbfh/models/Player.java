package com.bfh.djevtic.statisticsforbfh.models;

import java.util.ArrayList;

/**
 * Created by djevtic on 3.4.2015.
 */
public class Player {
    private String mGame;
    private String mPlatform;
    private String mName;
    private String mTAG;
    private long mDataCheck;
    private long mDataUpdate;
    private long mDataStreak;
    private long mDataCreated;
    private String mLastDay;
    private String mCountry;
    private String mCountryName;

    private long mScore;
    private long mTimePlayed;
    private String mUID;
    private String mUName;
    private String mUGava;
    private long mUDCreate;
    private String mPrivacy;
    private String mBLPlayer;
    private String mBLUser;
    private boolean mEditable;
    private boolean mViewable;
    private boolean mAdminable;
    private boolean mLinked;

    //COMPLEX OBJECTS
    private Rank mRank;
    private Dogtags mBasicDogtag;
    private Dogtags mAdvanceDogtag;
    private ArrayList<Weapons> mWeaponsList;
    private ArrayList<WeaponCategory> mWeaponCategoryList;
    private ArrayList<Vehicles> mVehicleList;
    private ArrayList<VehicleCategory> mVehicleCategoryList;
    private ArrayList<Awards> mAwardList;
    private ArrayList<Assighnments> mAssighnmentsList;
    private ArrayList<UpcomingUnlocks> mUpcomingUnlocksList;
    private Statistics mStatistics;

    public String getmGame() {
        return mGame;
    }

    public void setmGame(String mGame) {
        this.mGame = mGame;
    }

    public String getmPlatform() {
        return mPlatform;
    }

    public void setmPlatform(String mPlatform) {
        this.mPlatform = mPlatform;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public String getmTAG() {
        return mTAG;
    }

    public void setmTAG(String mTAG) {
        this.mTAG = mTAG;
    }

    public long getmDataCheck() {
        return mDataCheck;
    }

    public void setmDataCheck(long mDataCheck) {
        this.mDataCheck = mDataCheck;
    }

    public long getmDataUpdate() {
        return mDataUpdate;
    }

    public void setmDataUpdate(long mDataUpdate) {
        this.mDataUpdate = mDataUpdate;
    }

    public long getmDataStreak() {
        return mDataStreak;
    }

    public void setmDataStreak(long mDataStreak) {
        this.mDataStreak = mDataStreak;
    }

    public long getmDataCreated() {
        return mDataCreated;
    }

    public void setmDataCreated(long mDataCreated) {
        this.mDataCreated = mDataCreated;
    }

    public String getmLastDay() {
        return mLastDay;
    }

    public void setmLastDay(String mLastDay) {
        this.mLastDay = mLastDay;
    }

    public String getmCountry() {
        return mCountry;
    }

    public void setmCountry(String mCountry) {
        this.mCountry = mCountry;
    }

    public String getmCountryName() {
        return mCountryName;
    }

    public void setmCountryName(String mCountryName) {
        this.mCountryName = mCountryName;
    }

    public long getmScore() {
        return mScore;
    }

    public void setmScore(long mScore) {
        this.mScore = mScore;
    }

    public long getmTimePlayed() {
        return mTimePlayed;
    }

    public void setmTimePlayed(long mTimePlayed) {
        this.mTimePlayed = mTimePlayed;
    }

    public String getmUID() {
        return mUID;
    }

    public void setmUID(String mUID) {
        this.mUID = mUID;
    }

    public String getmUName() {
        return mUName;
    }

    public void setmUName(String mUName) {
        this.mUName = mUName;
    }

    public String getmUGava() {
        return mUGava;
    }

    public void setmUGava(String mUGava) {
        this.mUGava = mUGava;
    }

    public long getmUDCreate() {
        return mUDCreate;
    }

    public void setmUDCreate(long mUDCreate) {
        this.mUDCreate = mUDCreate;
    }

    public String getmPrivacy() {
        return mPrivacy;
    }

    public void setmPrivacy(String mPrivacy) {
        this.mPrivacy = mPrivacy;
    }

    public String getmBLPlayer() {
        return mBLPlayer;
    }

    public void setmBLPlayer(String mBLPlayer) {
        this.mBLPlayer = mBLPlayer;
    }

    public String getmBLUser() {
        return mBLUser;
    }

    public void setmBLUser(String mBLUser) {
        this.mBLUser = mBLUser;
    }

    public boolean ismEditable() {
        return mEditable;
    }

    public void setmEditable(boolean mEditable) {
        this.mEditable = mEditable;
    }

    public boolean ismViewable() {
        return mViewable;
    }

    public void setmViewable(boolean mViewable) {
        this.mViewable = mViewable;
    }

    public boolean ismAdminable() {
        return mAdminable;
    }

    public void setmAdminable(boolean mAdminable) {
        this.mAdminable = mAdminable;
    }

    public boolean ismLinked() {
        return mLinked;
    }

    public void setmLinked(boolean mLinked) {
        this.mLinked = mLinked;
    }

    public Rank getmRank() {
        return mRank;
    }

    public void setmRank(Rank mRank) {
        this.mRank = mRank;
    }

    public Dogtags getmBasicDogtag() {
        return mBasicDogtag;
    }

    public void setmBasicDogtag(Dogtags mBasicDogtag) {
        this.mBasicDogtag = mBasicDogtag;
    }

    public Dogtags getmAdvanceDogtag() {
        return mAdvanceDogtag;
    }

    public void setmAdvanceDogtag(Dogtags mAdvanceDogtag) {
        this.mAdvanceDogtag = mAdvanceDogtag;
    }

    public ArrayList<Weapons> getmWeaponsList() {
        return mWeaponsList;
    }

    public void setmWeaponsList(ArrayList<Weapons> mWeaponsList) {
        this.mWeaponsList = mWeaponsList;
    }

    public ArrayList<WeaponCategory> getmWeaponCategoryList() {
        return mWeaponCategoryList;
    }

    public void setmWeaponCategoryList(ArrayList<WeaponCategory> mWeaponCategoryList) {
        this.mWeaponCategoryList = mWeaponCategoryList;
    }

    public ArrayList<Vehicles> getmVehicleList() {
        return mVehicleList;
    }

    public void setmVehicleList(ArrayList<Vehicles> mVehicleList) {
        this.mVehicleList = mVehicleList;
    }

    public ArrayList<VehicleCategory> getmVehicleCategoryList() {
        return mVehicleCategoryList;
    }

    public void setmVehicleCategoryList(ArrayList<VehicleCategory> mVehicleCategoryList) {
        this.mVehicleCategoryList = mVehicleCategoryList;
    }

    public ArrayList<Awards> getmAwardList() {
        return mAwardList;
    }

    public void setmAwardList(ArrayList<Awards> mAwardList) {
        this.mAwardList = mAwardList;
    }

    public ArrayList<Assighnments> getmAssighnmentsList() {
        return mAssighnmentsList;
    }

    public void setmAssighnmentsList(ArrayList<Assighnments> mAssighnmentsList) {
        this.mAssighnmentsList = mAssighnmentsList;
    }

    public ArrayList<UpcomingUnlocks> getmUpcomingUnlocksList() {
        return mUpcomingUnlocksList;
    }

    public void setmUpcomingUnlocksList(ArrayList<UpcomingUnlocks> mUpcomingUnlocksList) {
        this.mUpcomingUnlocksList = mUpcomingUnlocksList;
    }

    public Statistics getmStatistics() {
        return mStatistics;
    }

    public void setmStatistics(Statistics mStatistics) {
        this.mStatistics = mStatistics;
    }
}
