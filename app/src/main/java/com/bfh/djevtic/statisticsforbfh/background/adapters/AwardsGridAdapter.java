package com.bfh.djevtic.statisticsforbfh.background.adapters;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import com.bfh.djevtic.statisticsforbfh.R;
import com.bfh.djevtic.statisticsforbfh.models.Awards;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by djevtic on 15.4.2015.
 */
public class AwardsGridAdapter extends ArrayAdapter {

    private Context mContext;
    private ArrayList<Awards> mAwards;

    public AwardsGridAdapter(Context context, int resource, ArrayList<Awards> awards) {
        super(context, resource, awards);
        this.mContext = context;
        this.mAwards = awards;
    }

    private static class ViewHolder {
        ImageView medal;
        ImageView ribon;
        TextView medalCount;
        TextView ribonCount;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder viewHolder;
        Awards awards = mAwards.get(position);
        if(convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.awards_item_grid_layout, null);
            }

            viewHolder = new ViewHolder();

            viewHolder.medal = (ImageView) convertView.findViewById(R.id.awards_medal_image);
            viewHolder.ribon = (ImageView) convertView.findViewById(R.id.awards_ribon_image);
            viewHolder.medalCount = (TextView) convertView.findViewById(R.id.awards_medal_count);
            viewHolder.ribonCount = (TextView) convertView.findViewById(R.id.awards_ribon_count);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        /*
        Have issue with all grayed after scroling till end of gridview
        if(awards.getmMedalCount() == 0) {
            viewHolder.medal.setAlpha(50);
        }
        if(awards.getmRibonCount() == 0){
            viewHolder.ribon.setAlpha(50);
        }
        */
        Picasso.with(mContext).load(mContext.getString(R.string.dropbox_url) + awards.getmMedalImg()).placeholder(R.drawable.maintenance).into(viewHolder.medal);
        Picasso.with(mContext).load(mContext.getString(R.string.dropbox_url) + awards.getmRibbonImg()).placeholder(R.drawable.maintenance).into(viewHolder.ribon);
        viewHolder.medalCount.setText(String.valueOf(awards.getmMedalCount()));
        viewHolder.ribonCount.setText(String.valueOf(awards.getmRibonCount()));
        return convertView;
    }
}
