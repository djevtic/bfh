package com.bfh.djevtic.statisticsforbfh.fragments.separatefragments;


import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bfh.djevtic.statisticsforbfh.R;
import com.bfh.djevtic.statisticsforbfh.background.adapters.ModesListAdapter;
import com.bfh.djevtic.statisticsforbfh.globals.Globals;
import com.bfh.djevtic.statisticsforbfh.models.Modes;

import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;

/**
 * Created by djevt_000 on 4/7/2015.
 */
public class ModesFragmentList extends ListFragment {

    private ArrayList<Modes> mModes;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mModes = Globals.player.getmStatistics().getmModes();
        setListAdapter(new ModesListAdapter(getActivity().getApplicationContext(), R.layout.modes_list_item_layout,mModes));

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mode_list_fragment, null, false);
        AdView mAdView = (AdView) view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        return view;
    }
}
