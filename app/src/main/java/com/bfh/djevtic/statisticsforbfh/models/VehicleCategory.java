package com.bfh.djevtic.statisticsforbfh.models;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by djevtic on 1.4.2015.
 */
public class VehicleCategory  implements Comparable<VehicleCategory> {
    private String mID;
    private String mName;
    private int mDestroys;
    private int mTime;
    private int mKills;
    private double mScore;
    private double mKPM;
    private double mSPM;

    public String getmID() {
        return mID;
    }

    public void setmID(String mID) {
        this.mID = mID;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public int getmDestroys() {
        return mDestroys;
    }

    public void setmDestroys(int mDestroys) {
        this.mDestroys = mDestroys;
    }

    public int getmTime() {
        return mTime;
    }

    public void setmTime(int mTime) {
        this.mTime = mTime;
    }

    public int getmKills() {
        return mKills;
    }

    public void setmKills(int mKills) {
        this.mKills = mKills;
    }

    public double getmScore() {
        return mScore;
    }

    public void setmScore(double mScore) {
        this.mScore = mScore;
    }

    public double getmKPM() {
        BigDecimal bd = new BigDecimal(mKPM);
        bd = bd.setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public void setmKPM(double mKPM) {
        this.mKPM = mKPM;
    }

    public double getmSPM() {
        BigDecimal bd = new BigDecimal(mSPM);
        bd = bd.setScale(4, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public void setmSPM(double mSPM) {
        this.mSPM = mSPM;
    }

    @Override
    public int compareTo(VehicleCategory another) {
        int compareQuantity = ((VehicleCategory) another).getmKills();
        return compareQuantity - this.getmKills();
    }
}
