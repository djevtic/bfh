package com.bfh.djevtic.statisticsforbfh.fragments.separatefragments;

import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.bfh.djevtic.statisticsforbfh.R;
import com.bfh.djevtic.statisticsforbfh.background.adapters.UpcomingListAdapter;
import com.bfh.djevtic.statisticsforbfh.globals.Globals;
import com.bfh.djevtic.statisticsforbfh.models.UpcomingUnlocks;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by djevtic on 15.4.2015.
 */
public class UpcomingUnlocksFragmentList extends ListFragment {
    private ArrayList<UpcomingUnlocks> mUpcomingUnlocks;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mUpcomingUnlocks = Globals.player.getmUpcomingUnlocksList();
        setListAdapter(new UpcomingListAdapter(getActivity().getApplicationContext(), R.layout.modes_list_item_layout, mUpcomingUnlocks));
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.mode_list_fragment, null, false);
        AdView mAdView = (AdView) view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        return view;
    }
}