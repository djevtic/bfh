package com.bfh.djevtic.statisticsforbfh.background;

import android.app.AlertDialog;
import android.app.ListActivity;
import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ListView;

import com.bfh.djevtic.statisticsforbfh.R;
import com.bfh.djevtic.statisticsforbfh.background.adapters.PLayerSelectLIstAdaptor;
import com.bfh.djevtic.statisticsforbfh.fragments.MainFragmentActivity;
import com.bfh.djevtic.statisticsforbfh.globals.Globals;
import com.bfh.djevtic.statisticsforbfh.models.Player;
import com.bfh.djevtic.statisticsforbfh.models.PlayerSelector;
import com.bfh.djevtic.statisticsforbfh.ui.PlayerSelect;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.HttpStatus;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.util.EntityUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by djevt_000 on 4/17/2015.
 */
public class PlayerSelectList extends ListActivity{

    private boolean fileReadable = false;
    private String temp="";
    private ArrayList<PlayerSelector> playerSelectList;
    private PLayerSelectLIstAdaptor adapter;
    private ProgressDialog mProgress;
    private boolean playerAdded = false;
    private String mUrl;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.player_list_layout);
        AdView mAdView = (AdView) findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        mProgress = new ProgressDialog(this);
        mProgress.setTitle("Loading");
        mProgress.setMessage("Wait while loading player data...");
    }

    @Override
    public void onResume() {
        super.onResume();

        read();
        Log.d("djevtic","File is readable: "+fileReadable);
        if(!fileReadable){
            Intent intent = new Intent(getApplicationContext(), PlayerSelect.class);
            intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            getApplicationContext().startActivity(intent);
        }else{
            playerSelectList = new ArrayList<>();
            try {
                JSONArray playerLIst = new JSONArray(temp);
                if(playerLIst.length()<1){
                    Intent intent = new Intent(getApplicationContext(), PlayerSelect.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(intent);
                }
                playerSelectList = getPlayerList(playerLIst);
                if(playerSelectList.size()<1){
                    Intent intent = new Intent(getApplicationContext(), PlayerSelect.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(intent);
                }
            } catch (JSONException e) {
                e.printStackTrace();
                Intent intent = new Intent(getApplicationContext(), PlayerSelect.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
            }
            adapter = new PLayerSelectLIstAdaptor(getApplicationContext(), R.layout.player_list_single_item,playerSelectList);
            ListView list = (ListView)findViewById(android.R.id.list);
            list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    Log.d("djevtic","We clicked on: "+playerSelectList.get(position).getmPLayerName());
                    fileReadable = false;
                    mUrl = playerSelectList.get(position).getmPlayerURL();
                    new DownloadFilesTask().execute();
                }
            });
            list.setAdapter(adapter);//setListAdapter(adapter);

            Button addPlayer = (Button) findViewById(R.id.player_list_add_player);
            addPlayer.setOnClickListener(new View.OnClickListener() {
                public void onClick(View v) {
                    Intent intent = new Intent(getApplicationContext(), PlayerSelect.class);
                    intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    getApplicationContext().startActivity(intent);
                }
            });
        }
        // Register mMessageReceiver to receive messages.
        LocalBroadcastManager.getInstance(this).registerReceiver(mMessageReceiver,
                new IntentFilter("my-event"));
    }

    /* Checks if external storage is available for read and write */
    public boolean isExternalStorageWritable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            return true;
        }
        return false;
    }

    /* Checks if external storage is available to at least read */
    public boolean isExternalStorageReadable() {
        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state) ||
                Environment.MEDIA_MOUNTED_READ_ONLY.equals(state)) {
            return true;
        }
        return false;
    }

    public void save(){
         String data = "Test Data";
        try {
            FileOutputStream fOut = openFileOutput(getString(R.string.player_list_file_name),MODE_WORLD_READABLE);
            fOut.write(data.getBytes());
            fOut.close();
            Log.d("djevtic", "Data saved ");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public void read(){
        temp = "";
        try{
            FileInputStream fin = openFileInput(getString(R.string.player_list_file_name));
            int c;
            while( (c = fin.read()) != -1){
                temp = temp + Character.toString((char)c);
            }
            Log.d("djevtic", "Read data in PLayerSelectList: "+temp);
            fileReadable = true;
        }catch(Exception e){
            e.printStackTrace();
        }
    }

    private ArrayList<PlayerSelector> getPlayerList(JSONArray playerJSONArray){
        ArrayList<PlayerSelector> playerList = new ArrayList<>();
        for(int i = 0;playerJSONArray.length()>i;i++) {
            PlayerSelector player = new PlayerSelector();
            try {
                JSONObject row = playerJSONArray.getJSONObject(i);
                player.setmPLayerName(row.getString("name"));
                player.setmPlayerURL(row.getString("url"));
                player.setmPlayerRank(row.getString("rankUrl"));
                playerList.add(player);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        return playerList;
    }

    private BroadcastReceiver mMessageReceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            // Extract data included in the Intent
            String message = intent.getStringExtra("message");
                for (int i = 0;playerSelectList.size()>i;i++){
                    if(playerSelectList.get(i).getmPLayerName().equals(message)){
                        playerSelectList.remove(i);
                        break;
                    }
                }
                JSONArray dataArray = new JSONArray(playerSelectList);
                save(dataArray.toString());
            if(playerSelectList.size()>0) {
                adapter.notifyDataSetChanged();
            }else{
                Intent playerSelectIntent = new Intent(getApplicationContext(), PlayerSelect.class);
                playerSelectIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(playerSelectIntent);
            }
            //}
            Log.d("djevtic", "Got message: " + message);
        }
    };

    private class DownloadFilesTask extends AsyncTask<Void, Void, Player> {

        @Override
        protected void onPreExecute() {
            mProgress.show();
        }

        protected Player doInBackground(Void... params) {
            HttpResponse response = setServerResponse(mUrl);
            Player player = getDataFromResponse(response);
            if(player!=null) {
                addPlayerToList(player);
            }

            return player;
        }


        protected void onPostExecute(Player player) {
            if (mProgress.isShowing()) {
                mProgress.dismiss();
            }
            if(player != null) {
                Globals.player = player;
                Intent intent;
                intent = new Intent(getApplicationContext(), MainFragmentActivity.class);
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                getApplicationContext().startActivity(intent);
            }else{
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                        PlayerSelectList.this);
                // set title
                alertDialogBuilder.setTitle(getString(R.string.player_select_alert_dialog_title));
                // set dialog message
                alertDialogBuilder
                        .setMessage(getString(R.string.player_select_alert_dialog_text))
                        .setCancelable(false)
                        .setNegativeButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                // if this button is clicked, just close
                                // the dialog box and do nothing
                                dialog.cancel();
                            }
                        });
                // create alert dialog
                AlertDialog alertDialog = alertDialogBuilder.create();
                // show it
                alertDialog.show();
            }
        }
    }

    private void addPlayerToList(Player player) {
        JSONArray playerList;
        read();
        if(fileReadable){
            try {
                playerList = new JSONArray(temp);
                for(int i = 0;playerList.length()>i;i++) {
                    try {
                        JSONObject row = playerList.getJSONObject(i);
                        if(player.getmName().equals(row.getString("name"))){
                            row.put("name",player.getmName());
                            row.put("url", mUrl);
                            row.put("rankUrl", player.getmRank().getmImage());
                            playerAdded = true;
                            break;
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                if(!playerAdded){
                    JSONObject playerJson = new JSONObject();
                    playerJson.put("name",player.getmName());
                    playerJson.put("url", mUrl);
                    playerJson.put("rankUrl", player.getmRank().getmImage());
                    playerList.put(playerJson);
                }
                save(playerList.toString());
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            playerList = new JSONArray();
            JSONObject playerJson = new JSONObject();
            try {
                playerJson.put("name",player.getmName());
                playerJson.put("url", mUrl);
                playerJson.put("rankUrl", player.getmRank().getmImage());
                playerList.put(playerJson);
            } catch (JSONException e) {
                e.printStackTrace();
            }
            save(playerList.toString());
        }
    }

    private HttpResponse setServerResponse(String url) {
        HttpClient client = new DefaultHttpClient();
        HttpGet request = new HttpGet(url);
        try {
            return client.execute(request);
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    private Player getDataFromResponse(HttpResponse response){
        if (response != null) {
            // Check for response status if all OK using response
            if (response.getStatusLine().getStatusCode() == HttpStatus.SC_OK) {
                HttpEntity entity = response.getEntity();
                try {
                    String data = EntityUtils.toString(entity);
                    JSONObject obj = new JSONObject(data);
                    PlayerFiller filler = new PlayerFiller(obj);
                    return filler.getmPlayer();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            } else {
                // In case response is incorrect use saved file if available
                //mJsonStore = fileUtils.readCachedText();
                Log.d("djevtic","Error response");
            }
        }
        return null;
    }

    public void save(String data){
        try {
            FileOutputStream fOut = openFileOutput(getString(R.string.player_list_file_name),MODE_WORLD_READABLE);
            fOut.write(data.getBytes());
            fOut.close();
            Log.d("djevtic", "Data saved ");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
}
