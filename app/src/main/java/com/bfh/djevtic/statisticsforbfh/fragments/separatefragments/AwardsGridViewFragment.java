package com.bfh.djevtic.statisticsforbfh.fragments.separatefragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.GridView;

import com.bfh.djevtic.statisticsforbfh.R;
import com.bfh.djevtic.statisticsforbfh.background.adapters.AwardsGridAdapter;
import com.bfh.djevtic.statisticsforbfh.background.adapters.WeaponListAdapter;
import com.bfh.djevtic.statisticsforbfh.globals.Globals;
import com.bfh.djevtic.statisticsforbfh.models.Awards;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.AdView;

import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by djevtic on 15.4.2015.
 */
public class AwardsGridViewFragment extends Fragment {

    private ArrayList<Awards> mAwards;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mAwards = Globals.player.getmAwardList();
        Collections.sort(mAwards);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.awards_layout_grid_view, container, false);
        GridView grid = (GridView) view.findViewById(R.id.gridview);
        grid.setAdapter(new AwardsGridAdapter(getActivity().getApplicationContext(), R.layout.awards_layout_grid_view, mAwards));
        AdView mAdView = (AdView) view.findViewById(R.id.adView);
        AdRequest adRequest = new AdRequest.Builder().build();
        mAdView.loadAd(adRequest);
        return view;
    }
}
