package com.bfh.djevtic.statisticsforbfh.models;

/**
 * Created by djevtic on 3.4.2015.
 */
public class Rank {
    private int mLevel;
    private String mImage;
    private String mName;
    private long mNeeded;
    private int mNextLevel;
    private String mNextImage;
    private String mNextName;
    private long mNextNeeded;
    private long mCurrent;
    private long mRealNeeded;
    private long mRealCurrent;
    private double mPercentage;

    public int getmLevel() {
        return mLevel;
    }

    public void setmLevel(int mLevel) {
        this.mLevel = mLevel;
    }

    public String getmImage() {
        return mImage;
    }

    public void setmImage(String mImage) {
        this.mImage = mImage;
    }

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public long getmNeeded() {
        return mNeeded;
    }

    public void setmNeeded(long mNeeded) {
        this.mNeeded = mNeeded;
    }

    public int getmNextLevel() {
        return mNextLevel;
    }

    public void setmNextLevel(int mNextLevel) {
        this.mNextLevel = mNextLevel;
    }

    public String getmNextImage() {
        return mNextImage;
    }

    public void setmNextImage(String mNextImage) {
        this.mNextImage = mNextImage;
    }

    public String getmNextName() {
        return mNextName;
    }

    public void setmNextName(String mNextName) {
        this.mNextName = mNextName;
    }

    public long getmNextNeeded() {
        return mNextNeeded;
    }

    public void setmNextNeeded(long mNextNeeded) {
        this.mNextNeeded = mNextNeeded;
    }

    public long getmCurrent() {
        return mCurrent;
    }

    public void setmCurrent(long mCurrent) {
        this.mCurrent = mCurrent;
    }

    public long getmRealNeeded() {
        return mRealNeeded;
    }

    public void setmRealNeeded(long mRealNeeded) {
        this.mRealNeeded = mRealNeeded;
    }

    public long getmRealCurrent() {
        return mRealCurrent;
    }

    public void setmRealCurrent(long mRealCurrent) {
        this.mRealCurrent = mRealCurrent;
    }

    public double getmPercentage() {
        return mPercentage;
    }

    public void setmPercentage(double mPercentage) {
        this.mPercentage = mPercentage;
    }
}