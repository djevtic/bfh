package com.bfh.djevtic.statisticsforbfh.models;

/**
 * Created by djevtic on 3.4.2015.
 */
public class Scores {
    private long mScore;
    private long mAwardScore;
    private long mBonusScore;
    private long mUnlockScore;
    private long mVehicleScore;
    private long mTeamScore;
    private long mObjectiveScore;
    private long mSquadScore;
    private long mHackerScore;
    private long mGeneralScore;
    private long mTotalScore;
    private long mRankScore;
    private long mCombatScore;

    public long getmScore() {
        return mScore;
    }

    public void setmScore(long mScore) {
        this.mScore = mScore;
    }

    public long getmAwardScore() {
        return mAwardScore;
    }

    public void setmAwardScore(long mAwardScore) {
        this.mAwardScore = mAwardScore;
    }

    public long getmBonusScore() {
        return mBonusScore;
    }

    public void setmBonusScore(long mBonusScore) {
        this.mBonusScore = mBonusScore;
    }

    public long getmUnlockScore() {
        return mUnlockScore;
    }

    public void setmUnlockScore(long mUnlockScore) {
        this.mUnlockScore = mUnlockScore;
    }

    public long getmVehicleScore() {
        return mVehicleScore;
    }

    public void setmVehicleScore(long mVehicleScore) {
        this.mVehicleScore = mVehicleScore;
    }

    public long getmTeamScore() {
        return mTeamScore;
    }

    public void setmTeamScore(long mTeamScore) {
        this.mTeamScore = mTeamScore;
    }

    public long getmObjectiveScore() {
        return mObjectiveScore;
    }

    public void setmObjectiveScore(long mObjectiveScore) {
        this.mObjectiveScore = mObjectiveScore;
    }

    public long getmSquadScore() {
        return mSquadScore;
    }

    public void setmSquadScore(long mSquadScore) {
        this.mSquadScore = mSquadScore;
    }

    public long getmHackerScore() {
        return mHackerScore;
    }

    public void setmHackerScore(long mHackerScore) {
        this.mHackerScore = mHackerScore;
    }

    public long getmGeneralScore() {
        return mGeneralScore;
    }

    public void setmGeneralScore(long mGeneralScore) {
        this.mGeneralScore = mGeneralScore;
    }

    public long getmTotalScore() {
        return mTotalScore;
    }

    public void setmTotalScore(long mTotalScore) {
        this.mTotalScore = mTotalScore;
    }

    public long getmRankScore() {
        return mRankScore;
    }

    public void setmRankScore(long mRankScore) {
        this.mRankScore = mRankScore;
    }

    public long getmCombatScore() {
        return mCombatScore;
    }

    public void setmCombatScore(long mCombatScore) {
        this.mCombatScore = mCombatScore;
    }
}