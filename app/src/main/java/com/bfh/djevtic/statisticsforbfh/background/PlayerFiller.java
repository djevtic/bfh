package com.bfh.djevtic.statisticsforbfh.background;

import android.util.Log;

import com.bfh.djevtic.statisticsforbfh.models.AssighnmentCriterias;
import com.bfh.djevtic.statisticsforbfh.models.AssighnmentDependencies;
import com.bfh.djevtic.statisticsforbfh.models.Assighnments;
import com.bfh.djevtic.statisticsforbfh.models.Awards;
import com.bfh.djevtic.statisticsforbfh.models.Dogtags;
import com.bfh.djevtic.statisticsforbfh.models.ExtrasScore;
import com.bfh.djevtic.statisticsforbfh.models.Kits;
import com.bfh.djevtic.statisticsforbfh.models.Modes;
import com.bfh.djevtic.statisticsforbfh.models.Player;
import com.bfh.djevtic.statisticsforbfh.models.Rank;
import com.bfh.djevtic.statisticsforbfh.models.Resets;
import com.bfh.djevtic.statisticsforbfh.models.Scores;
import com.bfh.djevtic.statisticsforbfh.models.Statistics;
import com.bfh.djevtic.statisticsforbfh.models.UpcomingUnlocks;
import com.bfh.djevtic.statisticsforbfh.models.VehicleCategory;
import com.bfh.djevtic.statisticsforbfh.models.Vehicles;
import com.bfh.djevtic.statisticsforbfh.models.WeaponCategory;
import com.bfh.djevtic.statisticsforbfh.models.Weapons;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.BufferedHttpEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;

/**
 * Created by djevtic on 3.4.2015.
 */
public class PlayerFiller {

    private static final String UPCOMING_UNLOCKS = "upcomingUnlocks";
    private static final String ASSIGHNMENTS = "assignments";
    private static final String AWARDS = "awards";
    private static final String VEHICLE_CATEGORY = "vehicleCategory";
    private static final String VEHICLES = "vehicles";
    private static final String WEAPON_CATEGORY_LIST = "weaponCategory";
    private static final String WEAPONS = "weapons";
    private static final String DOGTAGS = "dogtags";
    private static final String STATS = "stats";
    private static final String PLAYER = "player";

    private static final String TAG = "djevtic";

    private JSONObject mMainJsonObject;
    private Player mPlayer;

    public PlayerFiller(JSONObject json){
        this.mMainJsonObject = json;
        this.mPlayer = new Player();
        try {
            FillPLayerTop(mMainJsonObject.getJSONObject(PLAYER));
            FillStatistics(mMainJsonObject.getJSONObject(STATS));
            FillDogtags(mMainJsonObject.getJSONObject(DOGTAGS));
            FillWeapons(mMainJsonObject.getJSONArray(WEAPONS));
            FillWeaponCategory(mMainJsonObject.getJSONArray(WEAPON_CATEGORY_LIST));
            FillVehicles(mMainJsonObject.getJSONArray(VEHICLES));
            FillVehicleCategory(mMainJsonObject.getJSONArray(VEHICLE_CATEGORY));
            FillAwards(mMainJsonObject.getJSONArray(AWARDS));
            FillAssighnments(mMainJsonObject.getJSONArray(ASSIGHNMENTS));
            FillUpcomingUnlocks(mMainJsonObject.getJSONArray(UPCOMING_UNLOCKS));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        sendUpdateRequest();
    }

    private void sendUpdateRequest() {
        HttpClient httpclient = new DefaultHttpClient();
        String url = "http://bfhstats.com/"+getmPlayer().getmPlatform()+"/"+getmPlayer().getmName();
        Log.d(TAG,"Url for update: "+url.replaceAll(" ", "%20"));
        HttpGet httpget = new HttpGet(url.replaceAll(" ", "%20"));

        try {
            // Execute HTTP Post Request
            HttpResponse response = httpclient.execute(httpget);

            HttpEntity ht = response.getEntity();

            BufferedHttpEntity buf = new BufferedHttpEntity(ht);

            InputStream is = buf.getContent();

            BufferedReader r = new BufferedReader(new InputStreamReader(is));

            StringBuilder total = new StringBuilder();
            String line;
            while ((line = r.readLine()) != null) {
                total.append(line);
            }
            Log.d(TAG,"Response: "+response.getStatusLine().getStatusCode());
            Log.d(TAG,"From Request: "+line);

        } catch (ClientProtocolException e) {
            // TODO Auto-generated catch block
        } catch (IOException e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }

    public Player getmPlayer(){
        return mPlayer;
    }

    private void FillPLayerTop(JSONObject json){
        try {
            mPlayer.setmGame(json.getString("game"));
            mPlayer.setmPlatform(json.getString("plat"));
            mPlayer.setmName(json.getString("name"));
            mPlayer.setmTAG(json.getString("tag"));
            mPlayer.setmDataCheck(json.getLong("dateCheck"));
            mPlayer.setmDataUpdate(json.getLong("dateUpdate"));
            mPlayer.setmDataCreated(json.getLong("dateCreate"));
            mPlayer.setmDataStreak(json.getLong("dateStreak"));
            mPlayer.setmLastDay(json.getString("lastDay"));
            mPlayer.setmCountry(json.getString("country"));
            mPlayer.setmCountryName(json.getString("countryName"));
            mPlayer.setmScore(json.getLong("score"));
            mPlayer.setmTimePlayed(json.getLong("timePlayed"));
            mPlayer.setmUID(json.getString("uId"));
            mPlayer.setmUName(json.getString("uName"));
            mPlayer.setmUGava(json.getString("uGava"));
            mPlayer.setmUDCreate(json.getLong("udCreate"));
            mPlayer.setmPrivacy(json.getString("privacy"));
            mPlayer.setmBLPlayer(json.getString("blPlayer"));
            mPlayer.setmBLUser(json.getString("blUser"));
            mPlayer.setmEditable(json.getBoolean("editable"));
            mPlayer.setmViewable(json.getBoolean("viewable"));
            mPlayer.setmAdminable(json.getBoolean("adminable"));
            mPlayer.setmLinked(json.getBoolean("linked"));
            mPlayer.setmRank(FillRank(json.getJSONObject("rank")));
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

    private Rank FillRank(JSONObject json){
        Rank rank = new Rank();
        try {
            rank.setmLevel(json.getInt("nr"));
            rank.setmImage(json.getString("imgLarge"));
            rank.setmName(json.getString("name"));
            rank.setmNeeded(json.getLong("needed"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            JSONObject next = json.getJSONObject("next");
            rank.setmNextLevel(next.getInt("nr"));
            rank.setmNextImage(next.getString("imgLarge"));
            rank.setmNextName(next.getString("name"));
            rank.setmNextNeeded(next.getLong("needed"));
            rank.setmCurrent(next.getLong("curr"));
            rank.setmRealNeeded(next.getLong("relNeeded"));
            rank.setmRealCurrent(next.getLong("relCurr"));
            rank.setmPercentage(next.getDouble("relProg"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return rank;
    }

    private void FillStatistics(JSONObject statistics){
        Statistics stats = new Statistics();
        try {
            JSONObject reset = statistics.getJSONObject("reset");
            JSONObject scores = statistics.getJSONObject("scores");
            JSONArray modes = statistics.getJSONArray("modes");
            JSONObject kits = statistics.getJSONObject("kits");
            JSONObject extra = statistics.getJSONObject("extra");
            stats.setmKDRating(statistics.getDouble("kdRatio"));
            stats.setmAccuracy(statistics.getDouble("accuracy"));
            stats.setmSkill(statistics.getLong("skill"));
            stats.setmElo(statistics.getLong("elo"));
            stats.setmRank(statistics.getLong("rank"));
            stats.setmTimePlayed(statistics.getLong("timePlayed"));
            stats.setmKills(statistics.getLong("kills"));
            stats.setmDeaths(statistics.getLong("deaths"));
            stats.setmHeadshots(statistics.getLong("headshots"));
            stats.setmShootsFired(statistics.getLong("shotsFired"));
            stats.setmShootsHit(statistics.getLong("shotsHit"));
            stats.setmSuppressionAssist(statistics.getLong("suppressionAssists"));
            stats.setmAvengerKills(statistics.getLong("avengerKills"));
            stats.setmSaviourKills(statistics.getLong("saviorKills"));
            stats.setmNemesisKills(statistics.getLong("nemesisKills"));
            stats.setmNumberOfRounds(statistics.getInt("numRounds"));
            stats.setmNumberofLosses(statistics.getInt("numLosses"));
            stats.setmNumberOfWins(statistics.getInt("numWins"));
            stats.setmKillStreakBonus(statistics.getInt("killStreakBonus"));
            stats.setmNemesisKills(statistics.getInt("nemesisStreak"));
            stats.setMcomDefendKills(statistics.getInt("mcomDefendKills"));
            stats.setmResuplies(statistics.getInt("resupplies"));
            stats.setmRepaires(statistics.getInt("repairs"));
            stats.setmHeals(statistics.getInt("heals"));
            stats.setmRevives(statistics.getInt("revives"));
            stats.setmLongestHeadshot(statistics.getLong("longestHeadshot"));
            stats.setmLongestWinStreak(statistics.getInt("longestWinStreak"));
            stats.setmFlagDefend(statistics.getInt("flagDefend"));
            stats.setmFlagCaptures(statistics.getInt("flagCaptures"));
            stats.setmKillAssists(statistics.getInt("killAssists"));
            stats.setmVehicleDestroyed(statistics.getInt("vehiclesDestroyed"));
            stats.setmVehicleDamaged(statistics.getInt("vehicleDamage"));
            stats.setmDogtagsTaken(statistics.getInt("dogtagsTaken"));
            stats.setmStreak(statistics.getInt("streak"));
            stats.setmBestStreak(statistics.getInt("bestStreak"));

            //Complex data fill up
            stats.setmResets(FillResets(reset));
            stats.setmScores(FillScores(scores));
            stats.setmModes(FillModes(modes));
            stats.setmKits(FillKits(kits));
            stats.setmExtra(FillExtraScore(extra));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        mPlayer.setmStatistics(stats);
    }

    private ExtrasScore FillExtraScore(JSONObject extraScore){
        ExtrasScore score = new ExtrasScore();
        try {
            score.setmKDR(extraScore.getDouble("kdr"));
            score.setmWLR(extraScore.getDouble("wlr"));
            score.setmSPM(extraScore.getDouble("spm"));
            score.setmGSPM(extraScore.getDouble("gspm"));
            score.setmKPM(extraScore.getDouble("kpm"));
            score.setmSFPM(extraScore.getDouble("sfpm"));
            score.setmHKP(extraScore.getDouble("hkp"));
            score.setKHP(extraScore.getDouble("khp"));
            score.setmAccuracy(extraScore.getDouble("accuracy"));
            score.setmRoundFinished(extraScore.getInt("roundsFinished"));
            score.setmVehicleTime(extraScore.getInt("vehicleTime"));
            score.setmVehicleKills(extraScore.getInt("vehicleKills"));
            score.setmWeaponTime(extraScore.getInt("weaponTime"));
            score.setmWeaponKills(extraScore.getInt("weaponKills"));
            score.setmUnknownKills(extraScore.getInt("unknownKills"));
            score.setmWEATimeP(extraScore.getDouble("weaTimeP"));
            score.setmWEAKillsP(extraScore.getDouble("weaKillsP"));
            score.setmWEAKPM(extraScore.getDouble("weaKpm"));
            score.setmVehTimeP(extraScore.getDouble("vehTimeP"));
            score.setmVehKillsP(extraScore.getDouble("vehKillsP"));
            score.setmVehKPM(extraScore.getDouble("vehKpm"));
            score.setmRibbons(extraScore.getInt("ribbons"));
            score.setmRibbonsTotal(extraScore.getInt("ribbonsTotal"));
            score.setmRibbonsUnique(extraScore.getInt("ribbonsUnique"));
            score.setmMedals(extraScore.getInt("medals"));
            score.setmMedalsTotal(extraScore.getInt("medalsTotal"));
            score.setmMedalsUnique(extraScore.getInt("medalsUnique"));
            score.setmAssighnments(extraScore.getInt("assignments"));
            score.setmAssighnmentTota(extraScore.getInt("assignmentsTotal"));
            score.setmRIBPR(extraScore.getDouble("ribpr"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return score;
    }

    private ArrayList<Kits> FillKits(JSONObject kits){
        ArrayList<Kits> kitsList = new ArrayList<>();
        try {
            JSONObject operator = kits.getJSONObject("operator");
            JSONObject mechanic = kits.getJSONObject("mechanic");
            JSONObject enforcer = kits.getJSONObject("enforcer");
            JSONObject professional = kits.getJSONObject("professional");
            JSONObject commander = kits.getJSONObject("commander");
            kitsList.add(GetKitsData(operator));
            kitsList.add(GetKitsData(mechanic));
            kitsList.add(GetKitsData(enforcer));
            kitsList.add(GetKitsData(professional));
            kitsList.add(GetKitsData(commander));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return kitsList;
    }

    private Kits GetKitsData(JSONObject kitObject){
        Kits kit = new Kits();
        try {
            kit.setmID(kitObject.getInt("id"));
            kit.setmScore(kitObject.getInt("score"));
            kit.setmTime(kitObject.getInt("time"));
            kit.setmSPM(kitObject.getLong("spm"));
            kit.setmName(kitObject.getString("name"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return kit;
    }

    private ArrayList<Modes> FillModes(JSONArray modes){
        ArrayList<Modes> modesList = new ArrayList<>();
        for(int i = 0;modes.length()>i;i++) {
            Modes mode = new Modes();
            try {
                JSONObject row = modes.getJSONObject(i);
                mode.setmId(row.getString("id"));
                mode.setmScore(row.getInt("score"));
                mode.setmName(row.getString("name"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            modesList.add(mode);
        }
        return modesList;
    }

    private Scores FillScores(JSONObject scores){
        Scores score = new Scores();
        try {
            score.setmScore(scores.getLong("score"));
            score.setmAwardScore(scores.getLong("award"));
            score.setmBonusScore(scores.getLong("bonus"));
            score.setmUnlockScore(scores.getLong("unlock"));
            score.setmVehicleScore(scores.getLong("vehicle"));
            score.setmTeamScore(scores.getLong("team"));
            score.setmObjectiveScore(scores.getLong("objective"));
            score.setmSquadScore(scores.getLong("squad"));
            score.setmHackerScore(scores.getLong("hacker"));
            score.setmGeneralScore(scores.getLong("general"));
            score.setmTotalScore(scores.getLong("totalScore"));
            score.setmRankScore(scores.getLong("rankScore"));
            score.setmCombatScore(scores.getLong("combatScore"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return score;
    }

    private Resets FillResets(JSONObject resets){
        Resets reset = new Resets();
        try {
            reset.setmLastReset(resets.getLong("lastReset"));
            reset.setmScore(resets.getLong("score"));
            reset.setmTimePLayed(resets.getLong("timePlayed"));
            reset.setmTimePlayedSinceLastReset(resets.getLong("timePlayedSinceLastReset"));
            reset.setmKills(resets.getInt("kills"));
            reset.setmDeaths(resets.getInt("deaths"));
            reset.setmShotsFired(resets.getLong("shotsFired"));
            reset.setmShotsHit(resets.getLong("shotsHit"));
            reset.setmNumberLose(resets.getInt("numLosses"));
            reset.setmNumberWin(resets.getInt("numWins"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return  reset;
    }

    private void FillDogtags(JSONObject dogtags){
        try {
            JSONObject advancedJson = dogtags.getJSONObject("advanced");
            JSONObject basicJson = dogtags.getJSONObject("basic");
            mPlayer.setmAdvanceDogtag(FillDogtag(advancedJson));
            mPlayer.setmBasicDogtag(FillDogtag(basicJson));
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    private Dogtags FillDogtag(JSONObject dogtagObject){
        Dogtags dogtag = new Dogtags();
        try {
            dogtag.setmId(dogtagObject.getInt("id"));
            dogtag.setmImage(dogtagObject.getString("img"));
            dogtag.setmName(dogtagObject.getString("name"));
            dogtag.setmDescription(dogtagObject.getString("desc"));
            dogtag.setmCategory(dogtagObject.getString("category"));
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return dogtag;
    }

    private void FillWeapons(JSONArray weapons){
        ArrayList<Weapons> weaponsArrayList = new ArrayList<>();
        for(int i = 0;weapons.length()>i;i++){
           Weapons weapon = new Weapons();
            try {
                JSONObject row = weapons.getJSONObject(i);
                JSONObject stat = row.getJSONObject("stat");
                JSONObject detail = row.getJSONObject("detail");
                JSONObject extra = row.getJSONObject("extra");
                weapon.setmID(stat.getString("id"));
                weapon.setmShoots(stat.getInt("shots"));
                weapon.setmHits(stat.getInt("hits"));
                weapon.setmHeadShots(stat.getInt("hs"));
                weapon.setmKills(stat.getInt("kills"));
                weapon.setmTime(stat.getInt("time"));
                weapon.setmBStart(stat.getInt("bStars"));
                weapon.setmSStars(stat.getInt("sStars"));
                weapon.setmGStars(stat.getInt("gStars"));
                weapon.setmName(row.getString("name"));
                weapon.setmCategory(detail.getString("category"));
                weapon.setmType(detail.getString("type"));
                try {
                    weapon.setmDesscription(detail.getString("desc"));
                } catch (JSONException e) {
                    //e.printStackTrace();
                    weapon.setmDesscription("");
                }
                weapon.setmKillPerMinute(extra.getInt("kpm"));
                weapon.setmSFPM(extra.getDouble("sfpm"));
                weapon.setmHeadKillPercentage(extra.getDouble("hkp"));
                weapon.setmAccuracy(extra.getDouble("accuracy"));
                weapon.setmImageFancy(row.getString("imgFancy"));
                weapon.setmImageLineart(row.getString("imgLineart"));
                weaponsArrayList.add(weapon);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        mPlayer.setmWeaponsList(weaponsArrayList);
    }

    private void FillWeaponCategory(JSONArray weaponCategory){
        ArrayList<WeaponCategory> weaponCategoryList = new ArrayList<>();
        for(int i = 0;weaponCategory.length()>i;i++){
            WeaponCategory category = new WeaponCategory();
            try {
                JSONObject row = weaponCategory.getJSONObject(i);
                JSONObject stats = row.getJSONObject("stat");
                JSONObject extra = row.getJSONObject("extra");
                category.setmName(row.getString("name"));
                category.setmTime(stats.getInt("time"));
                category.setmKills(stats.getInt("kills"));
                category.setmHeadShots(stats.getInt("hs"));
                category.setmHits(stats.getInt("hits"));
                category.setmShots(stats.getInt("shots"));
                category.setmKPM(extra.getDouble("kpm"));
                category.setmSPM(extra.getDouble("spm"));
                category.setmSFPM(extra.getDouble("sfpm"));
                category.setmHeadKillPercentuage(extra.getDouble("hkp"));
                category.setmAccuracy(extra.getDouble("accuracy"));
                weaponCategoryList.add(category);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        mPlayer.setmWeaponCategoryList(weaponCategoryList);
    }


    private void FillVehicles(JSONArray vehicles){
        ArrayList<Vehicles> vehiclesList = new ArrayList<>();
        for(int i = 0;vehicles.length()>i;i++){
            Vehicles vehicle = new Vehicles();
            try {
                JSONObject row = vehicles.getJSONObject(i);
                JSONObject stats = row.getJSONObject("stat");
                JSONObject detail = row.getJSONObject("detail");
                JSONObject extra = row.getJSONObject("extra");
                vehicle.setmId(stats.getString("id"));
                vehicle.setmDestroys(stats.getInt("destroys"));
                vehicle.setmKills(stats.getInt("kills"));
                vehicle.setmTime(stats.getInt("time"));
                vehicle.setmBStars(stats.getInt("bStars"));
                vehicle.setmSStars(stats.getInt("sStars"));
                vehicle.setmGStars(stats.getInt("gStars"));
                vehicle.setmName(row.getString("name"));
                vehicle.setmCategory(detail.getString("category"));
                vehicle.setmKPM(extra.getDouble("kpm"));
                vehicle.setmImgFancy(row.getString("imgFancy"));
                vehicle.setmImgLineart(row.getString("imgLineart"));
                vehiclesList.add(vehicle);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        mPlayer.setmVehicleList(vehiclesList);
    }

    private void FillVehicleCategory(JSONArray vehicle){
        ArrayList<VehicleCategory> vehicleCategoryList = new ArrayList<>();
        for(int i = 0;vehicle.length()>i;i++){
            VehicleCategory category = new VehicleCategory();
            try {
                JSONObject row = vehicle.getJSONObject(i);
                JSONObject stat = row.getJSONObject("stat");
                JSONObject extra = row.getJSONObject("extra");
                category.setmName(row.getString("name"));
                category.setmDestroys(stat.getInt("destroys"));
                category.setmTime(stat.getInt("time"));
                category.setmKills(stat.getInt("kills"));
                category.setmKPM(extra.getDouble("kpm"));
                category.setmSPM(extra.getDouble("spm"));
            } catch (JSONException e) {
                e.printStackTrace();
            }
            vehicleCategoryList.add(category);
        }
        mPlayer.setmVehicleCategoryList(vehicleCategoryList);
    }

    private void FillAwards(JSONArray awards){
        ArrayList<Awards> awardsList = new ArrayList<>();
        for(int i = 0;awards.length()>i;i++){
            try {
                Awards award = new Awards();
                JSONObject row = awards.getJSONObject(i);
                JSONObject medal = row.getJSONObject("medal");
                JSONObject ribbon = row.getJSONObject("ribbon");
                award.setmId(row.getString("id"));
                award.setmMedalCount(row.getInt("mCount"));
                award.setmRibonCount(row.getInt("rCount"));
                award.setmMedalName(row.getString("mName"));
                award.setmRibonName(row.getString("rName"));
                award.setmMedalDescription(medal.getString("desc"));
                award.setmRibonDescription(ribbon.getString("desc"));
                award.setmRibonsNeeded(row.getInt("relNeeded"));
                award.setmMedalImg(row.getString("imgMedal"));
                award.setmRibbonImg(row.getString("imgRibbon"));
                awardsList.add(award);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        mPlayer.setmAwardList(awardsList);
    }

    private void FillAssighnments(JSONArray assighnments){
        ArrayList<Assighnments> asighnmentsList = new ArrayList<>();
        for(int i = 0;assighnments.length()>i;i++){
            Assighnments assignment = new Assighnments();
            try {
                JSONObject row = assighnments.getJSONObject(i);
                assignment.setmID(row.getString("id"));
                assignment.setmName(row.getString("name"));
                assignment.setmCategory(row.getString("category"));
                assignment.setmDescription(row.getString("desc"));
                assignment.setmCriterias(GetCriteriaList(row.getJSONArray("criterias")));
                assignment.setmDependencies(GetDependenciesList(row.getJSONArray("dependencies")));
                assignment.setmCurrent(row.getInt("curr"));
                assignment.setmNeeded(row.getInt("needed"));
                assignment.setmProg(row.getInt("prog"));
                assignment.setmImage(row.getString("img"));
                asighnmentsList.add(assignment);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        mPlayer.setmAssighnmentsList(asighnmentsList);
    }

    private ArrayList<AssighnmentDependencies> GetDependenciesList(JSONArray dependenciesArray){
        ArrayList<AssighnmentDependencies> dependencyList = new ArrayList<>();
        for(int i = 0;dependenciesArray.length()>i;i++){
            AssighnmentDependencies dependencie = new AssighnmentDependencies();
            try {
                JSONObject row = dependenciesArray.getJSONObject(i);
                dependencie.setmGroup(row.getString("group"));
                dependencie.setmRealm(row.getString("realm"));
                dependencie.setmCode(row.getString("code"));
                dependencie.setmName(row.getString("name"));
                dependencyList.add(dependencie);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(dependencyList.size()>0){
            return dependencyList;
        }else{
            return null;
        }
    }

    private ArrayList<AssighnmentCriterias> GetCriteriaList(JSONArray criteriaArray){
        ArrayList<AssighnmentCriterias> criteriaList = new ArrayList<>();
        for(int i = 0;criteriaArray.length()>i;i++){
            AssighnmentCriterias criteria = new AssighnmentCriterias();
            try {
                JSONObject row = criteriaArray.getJSONObject(i);
                criteria.setmDescription(row.getString("desc"));
                criteria.setmNeeded(row.getInt("needed"));
                criteria.setmCurrent(row.getInt("curr"));
                criteria.setmProg(row.getInt("prog"));
                criteriaList.add(criteria);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        if(criteriaList.size()>0){
            return criteriaList;
        }else{
            return null;
        }
    }



    private void FillUpcomingUnlocks(JSONArray unlocks){
        ArrayList<UpcomingUnlocks> upcomingUnlocksList = new ArrayList<>();
        for(int i = 0;unlocks.length()>i;i++){
            UpcomingUnlocks unlock = new UpcomingUnlocks();
            try {
                JSONObject row = unlocks.getJSONObject(i);
                unlock.setmName(row.getString("name"));
                unlock.setmType(row.getString("type"));
                unlock.setmSubName(row.getString("subname"));
                unlock.setmImage(row.getString("image"));
                unlock.setmSubImage(row.getString("subimage"));
                unlock.setmNeeded(row.getInt("needed"));
                unlock.setmFormat(row.getString("format"));
                unlock.setmNName(row.getString("nname"));
                unlock.setmCurr(row.getInt("curr"));
                unlock.setmProg(row.getInt("prog"));
                unlock.setmLeft(row.getInt("left"));
                try{
                    unlock.setmDescription(row.getString("desc"));
                }catch (JSONException e) {
                    unlock.setmDescription(null);
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            upcomingUnlocksList.add(unlock);
        }
        mPlayer.setmUpcomingUnlocksList(upcomingUnlocksList);
    }

}
