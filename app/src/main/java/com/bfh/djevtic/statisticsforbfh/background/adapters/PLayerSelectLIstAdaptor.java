package com.bfh.djevtic.statisticsforbfh.background.adapters;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.support.v4.content.LocalBroadcastManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bfh.djevtic.statisticsforbfh.R;
import com.bfh.djevtic.statisticsforbfh.background.PlayerSelectList;
import com.bfh.djevtic.statisticsforbfh.fragments.MainFragmentActivity;
import com.bfh.djevtic.statisticsforbfh.models.Modes;
import com.bfh.djevtic.statisticsforbfh.models.Player;
import com.bfh.djevtic.statisticsforbfh.models.PlayerSelector;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;

/**
 * Created by djevt_000 on 4/17/2015.
 */
public class PLayerSelectLIstAdaptor extends ArrayAdapter{

    private Context mContext;
    private ArrayList<PlayerSelector> mPlayerList;

    private String temp="";
    private boolean fileReadable = false;
    private boolean playerAdded = false;

    private static final String TAG = "djevtic";

    public PLayerSelectLIstAdaptor(Context context, int resource, ArrayList<PlayerSelector> playerList) {
        super(context, resource, playerList);
        this.mContext = context;
        this.mPlayerList = playerList;
    }

    private static class ViewHolder {
        TextView name;
        ImageView rank;
        ImageView button;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        Log.d(TAG, "ModesListAdapter: getView");
        ViewHolder viewHolder;
        final PlayerSelector player = mPlayerList.get(position);
        if(convertView == null) {
            LayoutInflater mInflater = (LayoutInflater) mContext
                    .getSystemService(Activity.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = mInflater.inflate(R.layout.player_list_single_item, null);
            }

            viewHolder = new ViewHolder();

            viewHolder.name = (TextView) convertView.findViewById(R.id.player_username);
            viewHolder.rank = (ImageView) convertView.findViewById(R.id.player_rank_image);
            viewHolder.button = (ImageView) convertView.findViewById(R.id.player_delete);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }
        viewHolder.name.setText(String.valueOf(player.getmPLayerName()));
        Log.d("djevtic","Link to image: "+mContext.getString(R.string.dropbox_url) + player.getmPlayerRank());
        Picasso.with(mContext).load(mContext.getString(R.string.dropbox_url) + player.getmPlayerRank()).placeholder(R.drawable.maintenance).into(viewHolder.rank);
        viewHolder.button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent("my-event");
                // add data
                intent.putExtra("message",player.getmPLayerName() );
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                //removePlayerFromList(player);
            }
        });
        return convertView;
    }

    private void removePlayerFromList(PlayerSelector player) {
        JSONArray playerList;
        ArrayList<PlayerSelector> list = new ArrayList<>();
        read();
        if(fileReadable){
            try {
                playerList = new JSONArray(temp);
                for(int i = 0;playerList.length()>i;i++) {
                    try {
                        JSONObject row = playerList.getJSONObject(i);
                        if(!player.getmPLayerName().equals(row.getString("name"))){
                            PlayerSelector selector = new PlayerSelector();
                            selector.setmPLayerName(row.getString("name"));
                            selector.setmPlayerRank(row.getString("rankUrl"));
                            selector.setmPlayerURL(row.getString("url"));
                            list.add(selector);
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                playerList = new JSONArray(list);
                save(playerList.toString());
                Intent intent = new Intent("my-event");
                // add data
                intent.putExtra("message", "update");
                LocalBroadcastManager.getInstance(mContext).sendBroadcast(intent);
                //Intent intent = new Intent(mContext, PlayerSelectList.class);
                //intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                //mContext.startActivity(intent);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public void save(String data){
        try {
            FileOutputStream fOut = mContext.openFileOutput(mContext.getString(R.string.player_list_file_name), mContext.MODE_WORLD_READABLE);
            fOut.write(data.getBytes());
            fOut.close();
            Log.d("djevtic", "Data saved in PLayerSelectListAdapter ");
        } catch (Exception e) {
            // TODO Auto-generated catch block
            e.printStackTrace();
        }
    }
    public void read(){
        try{
            FileInputStream fin = mContext.openFileInput(mContext.getString(R.string.player_list_file_name));
            int c;
            while( (c = fin.read()) != -1){
                temp = temp + Character.toString((char)c);
            }
            Log.d("djevtic", "Read data: "+temp);
            fileReadable = true;
        }catch(Exception e){
            e.printStackTrace();
        }
    }
}
