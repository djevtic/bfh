package com.bfh.djevtic.statisticsforbfh.models;

/**
 * Created by djevtic on 1.4.2015.
 */
public class AssighnmentCriterias {
    private String mDescription;
    private int mNeeded;
    private int mCurrent;
    private int mProg;

    public String getmDescription() {
        return mDescription;
    }

    public void setmDescription(String mDescription) {
        this.mDescription = mDescription;
    }

    public int getmNeeded() {
        return mNeeded;
    }

    public void setmNeeded(int mNeeded) {
        this.mNeeded = mNeeded;
    }

    public int getmCurrent() {
        return mCurrent;
    }

    public void setmCurrent(int mCurrent) {
        this.mCurrent = mCurrent;
    }

    public int getmProg() {
        return mProg;
    }

    public void setmProg(int mProg) {
        this.mProg = mProg;
    }
}